EESchema Schematic File Version 4
LIBS:BMS-Board-cache
EELAYER 26 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SFUSat-power:BQ25703ARSNR U1
U 1 1 5BC2582A
P 8350 8000
F 0 "U1" H 8350 10188 60  0001 C CNN
F 1 "BQ25703ARSNR" H 8350 8250 60  0000 C CNN
F 2 "SFUSat:BQ25703ARSNR" H 8350 7940 60  0001 C CNN
F 3 "" H 8350 8000 60  0000 C CNN
F 4 "296-47649-1-ND" H 8350 8000 50  0001 C CNN "Digikey #"
	1    8350 8000
	1    0    0    -1  
$EndComp
$Comp
L BMS-Board-rescue:VXO7803-500-VXO7803-500-BMS-Board-rescue-BMS-Board-rescue U2
U 1 1 5BC28193
P 9850 4050
F 0 "U2" H 9850 4517 50  0001 C CNN
F 1 "VXO7803-500" H 9850 4426 50  0000 C CNN
F 2 "SFUSat:CONV_VXO7803-500" H 9850 4050 50  0001 L BNN
F 3 "CUI Inc" H 9850 4050 50  0001 L BNN
F 4 "Manufacturer recommendations" H 9850 4050 50  0001 L BNN "Field4"
F 5 "1.0" H 9850 4050 50  0001 L BNN "Field5"
F 6 "102-4248-ND" H 9850 4050 50  0001 C CNN "Digikey #"
	1    9850 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5BCCE1B6
P 6050 7700
F 0 "#PWR02" H 6050 7450 50  0001 C CNN
F 1 "GND" H 6055 7572 50  0001 R CNN
F 2 "" H 6050 7700 50  0001 C CNN
F 3 "" H 6050 7700 50  0001 C CNN
	1    6050 7700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5BCCE332
P 6200 6250
F 0 "#PWR03" H 6200 6000 50  0001 C CNN
F 1 "GND" H 6205 6122 50  0001 R CNN
F 2 "" H 6200 6250 50  0001 C CNN
F 3 "" H 6200 6250 50  0001 C CNN
	1    6200 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5700 6500 7000
Wire Wire Line
	6600 5700 6500 5700
$Comp
L power:GND #PWR04
U 1 1 5BCCE80E
P 6600 6000
F 0 "#PWR04" H 6600 5750 50  0001 C CNN
F 1 "GND" H 6605 5872 50  0001 R CNN
F 2 "" H 6600 6000 50  0001 C CNN
F 3 "" H 6600 6000 50  0001 C CNN
	1    6600 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5BCCE9B2
P 6750 5550
F 0 "#PWR05" H 6750 5300 50  0001 C CNN
F 1 "GND" H 6755 5422 50  0001 R CNN
F 2 "" H 6750 5550 50  0001 C CNN
F 3 "" H 6750 5550 50  0001 C CNN
	1    6750 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BCCED9C
P 3450 5650
F 0 "#PWR01" H 3450 5400 50  0001 C CNN
F 1 "GND" H 3455 5477 50  0001 C CNN
F 2 "" H 3450 5650 50  0001 C CNN
F 3 "" H 3450 5650 50  0001 C CNN
	1    3450 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 6400 7400 6400
Wire Wire Line
	9250 6400 9300 6400
Wire Wire Line
	6050 7400 7450 7400
Wire Wire Line
	6350 7200 7450 7200
Wire Wire Line
	6500 7000 7450 7000
Wire Wire Line
	6900 6800 7450 6800
Connection ~ 9300 5250
$Comp
L power:GND #PWR012
U 1 1 5BCE6ABC
P 9250 8400
F 0 "#PWR012" H 9250 8150 50  0001 C CNN
F 1 "GND" V 9255 8272 50  0001 R CNN
F 2 "" H 9250 8400 50  0001 C CNN
F 3 "" H 9250 8400 50  0001 C CNN
	1    9250 8400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9550 8700 9250 8700
Text GLabel 9850 8700 2    50   Input ~ 0
VDDA
$Comp
L power:GND #PWR014
U 1 1 5BCE9157
P 9550 9400
F 0 "#PWR014" H 9550 9150 50  0001 C CNN
F 1 "GND" V 9555 9272 50  0001 R CNN
F 2 "" H 9550 9400 50  0001 C CNN
F 3 "" H 9550 9400 50  0001 C CNN
	1    9550 9400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5BCE91FF
P 9550 9200
F 0 "#PWR013" H 9550 8950 50  0001 C CNN
F 1 "GND" V 9555 9072 50  0001 R CNN
F 2 "" H 9550 9200 50  0001 C CNN
F 3 "" H 9550 9200 50  0001 C CNN
	1    9550 9200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5BCEBB1E
P 9900 9300
F 0 "#PWR016" H 9900 9050 50  0001 C CNN
F 1 "GND" V 9905 9172 50  0001 R CNN
F 2 "" H 9900 9300 50  0001 C CNN
F 3 "" H 9900 9300 50  0001 C CNN
	1    9900 9300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 8000 7450 7900
Wire Wire Line
	7450 7600 7150 7600
Wire Wire Line
	7150 7600 7150 7650
$Comp
L power:GND #PWR08
U 1 1 5BCF0123
P 7150 8000
F 0 "#PWR08" H 7150 7750 50  0001 C CNN
F 1 "GND" V 7155 7872 50  0001 R CNN
F 2 "" H 7150 8000 50  0001 C CNN
F 3 "" H 7150 8000 50  0001 C CNN
	1    7150 8000
	1    0    0    -1  
$EndComp
Text GLabel 6850 7600 0    50   Input ~ 0
REGN
Wire Wire Line
	9250 8100 9450 8100
$Comp
L power:GND #PWR015
U 1 1 5BCF52D7
P 9900 8250
F 0 "#PWR015" H 9900 8000 50  0001 C CNN
F 1 "GND" V 9905 8122 50  0001 R CNN
F 2 "" H 9900 8250 50  0001 C CNN
F 3 "" H 9900 8250 50  0001 C CNN
	1    9900 8250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7450 8300 7450 8150
$Comp
L power:GND #PWR06
U 1 1 5BD04AE5
P 6850 8150
F 0 "#PWR06" H 6850 7900 50  0001 C CNN
F 1 "GND" V 6855 8022 50  0001 R CNN
F 2 "" H 6850 8150 50  0001 C CNN
F 3 "" H 6850 8150 50  0001 C CNN
	1    6850 8150
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 9600 7400 9600
Wire Wire Line
	7400 9600 7400 10100
Wire Wire Line
	7450 9400 7350 9400
Wire Wire Line
	7450 9200 7300 9200
Wire Wire Line
	7450 9000 7250 9000
NoConn ~ 7200 9000
NoConn ~ 7250 9200
NoConn ~ 7200 9200
NoConn ~ 7350 9600
NoConn ~ 7300 9600
NoConn ~ 7250 9600
NoConn ~ 7200 9600
Wire Wire Line
	10250 8700 10250 7900
Wire Wire Line
	10250 7900 9250 7900
Wire Wire Line
	10250 9600 9300 9600
$Comp
L power:GND #PWR017
U 1 1 5BD57A46
P 10250 8900
F 0 "#PWR017" H 10250 8650 50  0001 C CNN
F 1 "GND" V 10255 8772 50  0001 R CNN
F 2 "" H 10250 8900 50  0001 C CNN
F 3 "" H 10250 8900 50  0001 C CNN
	1    10250 8900
	0    1    1    0   
$EndComp
Wire Wire Line
	9150 3950 8950 3950
Wire Wire Line
	10550 4050 10550 4450
Wire Wire Line
	10550 4450 9150 4450
Wire Wire Line
	10650 4450 10550 4450
Connection ~ 10550 4450
Wire Wire Line
	10650 3950 10550 3950
Connection ~ 10650 3950
$Comp
L power:GND #PWR011
U 1 1 5BD7032A
P 9150 4450
F 0 "#PWR011" H 9150 4200 50  0001 C CNN
F 1 "GND" H 9155 4277 50  0001 C CNN
F 2 "" H 9150 4450 50  0001 C CNN
F 3 "" H 9150 4450 50  0001 C CNN
	1    9150 4450
	1    0    0    -1  
$EndComp
Connection ~ 9150 4450
Wire Wire Line
	9150 4450 8950 4450
Wire Wire Line
	7200 10300 11200 10300
Wire Wire Line
	7250 10250 11150 10250
Wire Wire Line
	7300 10200 11100 10200
Wire Wire Line
	7350 10150 11050 10150
Wire Wire Line
	7400 10100 11000 10100
$Comp
L power:GND #PWR020
U 1 1 5BD9DDBC
P 13550 5100
F 0 "#PWR020" H 13550 4850 50  0001 C CNN
F 1 "GND" H 13555 4927 50  0001 C CNN
F 2 "" H 13550 5100 50  0001 C CNN
F 3 "" H 13550 5100 50  0001 C CNN
	1    13550 5100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5BD9DF2F
P 10650 3950
F 0 "TP11" H 10708 4024 50  0000 L CNN
F 1 "TestPoint" H 10708 3979 50  0001 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Big" H 10850 3950 50  0001 C CNN
F 3 "~" H 10850 3950 50  0001 C CNN
	1    10650 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5BD9E437
P 9550 8700
F 0 "TP10" H 9492 8773 50  0000 R CNN
F 1 "TestPoint" H 9608 8729 50  0001 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Big" H 9750 8700 50  0001 C CNN
F 3 "~" H 9750 8700 50  0001 C CNN
	1    9550 8700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5BE2DD79
P 13550 5700
F 0 "#PWR021" H 13550 5450 50  0001 C CNN
F 1 "GND" H 13555 5527 50  0000 C CNN
F 2 "" H 13550 5700 50  0001 C CNN
F 3 "" H 13550 5700 50  0001 C CNN
	1    13550 5700
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-power:1935161 J5
U 1 1 5BE337EE
P 13850 5600
F 0 "J5" H 13980 5646 50  0000 L CNN
F 1 "1935161" H 13980 5555 50  0000 L CNN
F 2 "SFUSat:PHOENIX_1935161" H 13850 5600 50  0001 L BNN
F 3 "https://www.digikey.ca/product-detail/en/phoenix-contact/1935161/277-1667-ND/568614?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 13850 5600 50  0001 L BNN
F 4 "1935161" H 13850 5600 50  0001 L BNN "Field4"
F 5 "None" H 13850 5600 50  0001 L BNN "Field5"
F 6 "Conn Terminal Blocks 2 POS 5mm Solder ST Thru-Hole 16A" H 13850 5600 50  0001 L BNN "Field6"
F 7 "277-1667-ND" H 13850 5600 50  0001 L BNN "Digikey #"
F 8 "Phoenix Contact" H 13850 5600 50  0001 L BNN "Field8"
	1    13850 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5250 9500 5250
Wire Wire Line
	11000 10100 11000 8650
Wire Wire Line
	12850 8350 12850 8400
Wire Wire Line
	11100 8550 11150 8550
Wire Wire Line
	13050 8500 11200 8500
NoConn ~ 11200 8550
NoConn ~ 11150 8550
$Comp
L power:GND #PWR019
U 1 1 5BF1B261
P 13350 6150
F 0 "#PWR019" H 13350 5900 50  0001 C CNN
F 1 "GND" H 13355 5977 50  0000 C CNN
F 2 "" H 13350 6150 50  0001 C CNN
F 3 "" H 13350 6150 50  0001 C CNN
	1    13350 6150
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-power:1935161 J4
U 1 1 5BEFAC74
P 13850 5000
F 0 "J4" H 13980 5046 50  0000 L CNN
F 1 "1935161" H 13980 4955 50  0000 L CNN
F 2 "SFUSat:PHOENIX_1935161" H 13850 5000 50  0001 L BNN
F 3 "https://www.digikey.ca/product-detail/en/phoenix-contact/1935161/277-1667-ND/568614?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 13850 5000 50  0001 L BNN
F 4 "1935161" H 13850 5000 50  0001 L BNN "Field4"
F 5 "None" H 13850 5000 50  0001 L BNN "Field5"
F 6 "Conn Terminal Blocks 2 POS 5mm Solder ST Thru-Hole 16A" H 13850 5000 50  0001 L BNN "Field6"
F 7 "277-1667-ND" H 13850 5000 50  0001 L BNN "Digikey #"
F 8 "Phoenix Contact" H 13850 5000 50  0001 L BNN "Field8"
	1    13850 5000
	1    0    0    -1  
$EndComp
Text GLabel 11000 3950 2    50   Output ~ 0
3v3
Wire Wire Line
	10650 3950 11000 3950
Text GLabel 13050 6150 1    50   Input ~ 0
3v3
Text GLabel 6400 8600 0    50   Input ~ 0
3v3
Text Label 13300 4900 3    50   ~ 0
VSYS
Text Label 13300 5500 3    50   ~ 0
VBAT
Text Label 10950 3950 3    50   ~ 0
3v3
Text Label 3550 5250 3    50   ~ 0
VIN
Connection ~ 7200 8800
Wire Wire Line
	7200 8800 7450 8800
Connection ~ 7250 9000
Connection ~ 7300 9200
Connection ~ 7400 9600
Wire Wire Line
	7350 9400 7350 9600
Wire Wire Line
	7300 9200 7300 9600
Wire Wire Line
	7250 9000 7250 9200
Wire Wire Line
	7200 8800 7200 9000
Text GLabel 10400 7900 2    50   Output ~ 0
REGN
Wire Wire Line
	10250 7900 10400 7900
Connection ~ 10250 7900
Text GLabel 7400 7500 0    50   Output ~ 0
VDDA
Wire Wire Line
	7450 7500 7450 7600
Connection ~ 7450 7600
Wire Wire Line
	7400 7500 7450 7500
Wire Wire Line
	9250 9800 9300 9800
Wire Wire Line
	9300 9800 9300 9600
Connection ~ 9300 9600
Wire Wire Line
	9300 9600 9250 9600
$Comp
L power:GND #PWR09
U 1 1 5BFB00CC
P 7450 9800
F 0 "#PWR09" H 7450 9550 50  0001 C CNN
F 1 "GND" V 7455 9672 50  0001 R CNN
F 2 "" H 7450 9800 50  0001 C CNN
F 3 "" H 7450 9800 50  0001 C CNN
	1    7450 9800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5BFBB262
P 11450 8100
F 0 "#PWR018" H 11450 7850 50  0001 C CNN
F 1 "GND" V 11455 7972 50  0001 R CNN
F 2 "" H 11450 8100 50  0001 C CNN
F 3 "" H 11450 8100 50  0001 C CNN
	1    11450 8100
	-1   0    0    1   
$EndComp
Wire Wire Line
	11500 7400 11500 6050
Wire Wire Line
	12000 7600 12000 6050
Wire Wire Line
	9250 7400 11500 7400
Wire Wire Line
	9250 7600 12000 7600
Wire Wire Line
	9250 7200 10050 7200
Wire Wire Line
	10950 5500 10950 5600
Wire Wire Line
	9250 6600 9400 6600
Wire Wire Line
	6950 6600 7450 6600
Wire Wire Line
	9250 5950 9250 6200
Wire Wire Line
	7450 6200 7450 5950
Wire Wire Line
	7400 5250 7400 6400
Connection ~ 7400 5250
Wire Wire Line
	7250 5250 7400 5250
Wire Wire Line
	13350 6150 13550 6150
Wire Wire Line
	8050 5550 8200 5550
Wire Wire Line
	8200 5550 8200 5950
Wire Wire Line
	8650 5550 8500 5550
Wire Wire Line
	8500 5550 8500 5950
Wire Wire Line
	8500 5950 9250 5950
Wire Wire Line
	7450 5950 8200 5950
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C1
U 1 1 5C02399C
P 3800 5450
F 0 "C1" V 3548 5450 50  0000 C CNN
F 1 "C_10u0_10%_25V_X5R_0805" V 3639 5450 50  0000 C CNN
F 2 "SFUSat-cap:C_0805" H 3800 5450 50  0001 C CNN
F 3 "" H 3800 5450 50  0001 C CNN
F 4 "General Purpose" H 3800 5450 50  0001 C CNN "Applications"
F 5 "10µF" H 3800 5450 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 3800 5450 50  0001 C CNN "Categories"
F 7 "-" H 3800 5450 50  0001 C CNN "Features"
F 8 "-" H 3800 5450 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 3800 5450 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 3800 5450 50  0001 C CNN "Lead Spacing"
F 11 "-" H 3800 5450 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 3800 5450 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 3800 5450 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 3800 5450 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 3800 5450 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 3800 5450 50  0001 C CNN "Package / Case"
F 17 "Active" H 3800 5450 50  0001 C CNN "Part Status"
F 18 "-" H 3800 5450 50  0001 C CNN "Ratings"
F 19 "GRM" H 3800 5450 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 3800 5450 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 3800 5450 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 3800 5450 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 3800 5450 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 3800 5450 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 3800 5450 50  0001 C CNN "Tolerance"
F 26 "25V" H 3800 5450 50  0001 C CNN "Voltage - Rated"
	1    3800 5450
	0    1    1    0   
$EndComp
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C2
U 1 1 5C023A90
P 3800 5650
F 0 "C2" V 3548 5650 50  0000 C CNN
F 1 "C_10u0_10%_25V_X5R_0805" V 3639 5650 50  0000 C CNN
F 2 "SFUSat-cap:C_0805" H 3800 5650 50  0001 C CNN
F 3 "" H 3800 5650 50  0001 C CNN
F 4 "General Purpose" H 3800 5650 50  0001 C CNN "Applications"
F 5 "10µF" H 3800 5650 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 3800 5650 50  0001 C CNN "Categories"
F 7 "-" H 3800 5650 50  0001 C CNN "Features"
F 8 "-" H 3800 5650 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 3800 5650 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 3800 5650 50  0001 C CNN "Lead Spacing"
F 11 "-" H 3800 5650 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 3800 5650 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 3800 5650 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 3800 5650 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 3800 5650 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 3800 5650 50  0001 C CNN "Package / Case"
F 17 "Active" H 3800 5650 50  0001 C CNN "Part Status"
F 18 "-" H 3800 5650 50  0001 C CNN "Ratings"
F 19 "GRM" H 3800 5650 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 3800 5650 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 3800 5650 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 3800 5650 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 3800 5650 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 3800 5650 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 3800 5650 50  0001 C CNN "Tolerance"
F 26 "25V" H 3800 5650 50  0001 C CNN "Voltage - Rated"
	1    3800 5650
	0    1    1    0   
$EndComp
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C3
U 1 1 5C023B0A
P 3800 5850
F 0 "C3" V 3548 5850 50  0000 C CNN
F 1 "C_10u0_10%_25V_X5R_0805" V 3639 5850 50  0000 C CNN
F 2 "SFUSat-cap:C_0805" H 3800 5850 50  0001 C CNN
F 3 "" H 3800 5850 50  0001 C CNN
F 4 "General Purpose" H 3800 5850 50  0001 C CNN "Applications"
F 5 "10µF" H 3800 5850 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 3800 5850 50  0001 C CNN "Categories"
F 7 "-" H 3800 5850 50  0001 C CNN "Features"
F 8 "-" H 3800 5850 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 3800 5850 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 3800 5850 50  0001 C CNN "Lead Spacing"
F 11 "-" H 3800 5850 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 3800 5850 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 3800 5850 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 3800 5850 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 3800 5850 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 3800 5850 50  0001 C CNN "Package / Case"
F 17 "Active" H 3800 5850 50  0001 C CNN "Part Status"
F 18 "-" H 3800 5850 50  0001 C CNN "Ratings"
F 19 "GRM" H 3800 5850 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 3800 5850 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 3800 5850 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 3800 5850 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 3800 5850 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 3800 5850 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 3800 5850 50  0001 C CNN "Tolerance"
F 26 "25V" H 3800 5850 50  0001 C CNN "Voltage - Rated"
	1    3800 5850
	0    1    1    0   
$EndComp
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C4
U 1 1 5C023B7E
P 3800 6050
F 0 "C4" V 3548 6050 50  0000 C CNN
F 1 "C_10u0_10%_25V_X5R_0805" V 3639 6050 50  0000 C CNN
F 2 "SFUSat-cap:C_0805" H 3800 6050 50  0001 C CNN
F 3 "" H 3800 6050 50  0001 C CNN
F 4 "General Purpose" H 3800 6050 50  0001 C CNN "Applications"
F 5 "10µF" H 3800 6050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 3800 6050 50  0001 C CNN "Categories"
F 7 "-" H 3800 6050 50  0001 C CNN "Features"
F 8 "-" H 3800 6050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 3800 6050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 3800 6050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 3800 6050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 3800 6050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 3800 6050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 3800 6050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 3800 6050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 3800 6050 50  0001 C CNN "Package / Case"
F 17 "Active" H 3800 6050 50  0001 C CNN "Part Status"
F 18 "-" H 3800 6050 50  0001 C CNN "Ratings"
F 19 "GRM" H 3800 6050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 3800 6050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 3800 6050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 3800 6050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 3800 6050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 3800 6050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 3800 6050 50  0001 C CNN "Tolerance"
F 26 "25V" H 3800 6050 50  0001 C CNN "Voltage - Rated"
	1    3800 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 5450 3450 5650
Wire Wire Line
	3450 5450 3650 5450
Wire Wire Line
	3650 6050 3650 5850
Wire Wire Line
	3650 5850 3650 5650
Connection ~ 3650 5850
Wire Wire Line
	3650 5650 3650 5450
Connection ~ 3650 5650
Connection ~ 3650 5450
Wire Wire Line
	3450 5250 3950 5250
Wire Wire Line
	3950 5250 3950 5450
Wire Wire Line
	3950 5450 3950 5650
Connection ~ 3950 5450
Wire Wire Line
	3950 5650 3950 5850
Connection ~ 3950 5650
Wire Wire Line
	3950 5850 3950 6050
Connection ~ 3950 5850
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C21
U 1 1 5C09D884
P 11400 5050
F 0 "C21" H 11285 5004 50  0000 R CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 11285 5095 50  0000 R CNN
F 2 "SFUSat-cap:C_0805" H 11400 5050 50  0001 C CNN
F 3 "" H 11400 5050 50  0001 C CNN
F 4 "General Purpose" H 11400 5050 50  0001 C CNN "Applications"
F 5 "10µF" H 11400 5050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 11400 5050 50  0001 C CNN "Categories"
F 7 "-" H 11400 5050 50  0001 C CNN "Features"
F 8 "-" H 11400 5050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11400 5050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 11400 5050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 11400 5050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 11400 5050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 11400 5050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 11400 5050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 11400 5050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 11400 5050 50  0001 C CNN "Package / Case"
F 17 "Active" H 11400 5050 50  0001 C CNN "Part Status"
F 18 "-" H 11400 5050 50  0001 C CNN "Ratings"
F 19 "GRM" H 11400 5050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 11400 5050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 11400 5050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 11400 5050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 11400 5050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 11400 5050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 11400 5050 50  0001 C CNN "Tolerance"
F 26 "25V" H 11400 5050 50  0001 C CNN "Voltage - Rated"
	1    11400 5050
	-1   0    0    1   
$EndComp
Connection ~ 11400 4900
Wire Wire Line
	11400 4900 11650 4900
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C22
U 1 1 5C0A7D88
P 11650 5050
F 0 "C22" H 11535 5004 50  0000 R CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 11535 5095 50  0000 R CNN
F 2 "SFUSat-cap:C_0805" H 11650 5050 50  0001 C CNN
F 3 "" H 11650 5050 50  0001 C CNN
F 4 "General Purpose" H 11650 5050 50  0001 C CNN "Applications"
F 5 "10µF" H 11650 5050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 11650 5050 50  0001 C CNN "Categories"
F 7 "-" H 11650 5050 50  0001 C CNN "Features"
F 8 "-" H 11650 5050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11650 5050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 11650 5050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 11650 5050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 11650 5050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 11650 5050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 11650 5050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 11650 5050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 11650 5050 50  0001 C CNN "Package / Case"
F 17 "Active" H 11650 5050 50  0001 C CNN "Part Status"
F 18 "-" H 11650 5050 50  0001 C CNN "Ratings"
F 19 "GRM" H 11650 5050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 11650 5050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 11650 5050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 11650 5050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 11650 5050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 11650 5050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 11650 5050 50  0001 C CNN "Tolerance"
F 26 "25V" H 11650 5050 50  0001 C CNN "Voltage - Rated"
	1    11650 5050
	-1   0    0    1   
$EndComp
Connection ~ 11650 4900
Wire Wire Line
	11650 4900 11900 4900
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C24
U 1 1 5C0A7E08
P 11900 5050
F 0 "C24" H 11785 5004 50  0000 R CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 11785 5095 50  0000 R CNN
F 2 "SFUSat-cap:C_0805" H 11900 5050 50  0001 C CNN
F 3 "" H 11900 5050 50  0001 C CNN
F 4 "General Purpose" H 11900 5050 50  0001 C CNN "Applications"
F 5 "10µF" H 11900 5050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 11900 5050 50  0001 C CNN "Categories"
F 7 "-" H 11900 5050 50  0001 C CNN "Features"
F 8 "-" H 11900 5050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11900 5050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 11900 5050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 11900 5050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 11900 5050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 11900 5050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 11900 5050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 11900 5050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 11900 5050 50  0001 C CNN "Package / Case"
F 17 "Active" H 11900 5050 50  0001 C CNN "Part Status"
F 18 "-" H 11900 5050 50  0001 C CNN "Ratings"
F 19 "GRM" H 11900 5050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 11900 5050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 11900 5050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 11900 5050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 11900 5050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 11900 5050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 11900 5050 50  0001 C CNN "Tolerance"
F 26 "25V" H 11900 5050 50  0001 C CNN "Voltage - Rated"
	1    11900 5050
	-1   0    0    1   
$EndComp
Connection ~ 11900 4900
Wire Wire Line
	11900 4900 12150 4900
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C25
U 1 1 5C0A7E8A
P 12150 5050
F 0 "C25" H 12035 5004 50  0000 R CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 12035 5095 50  0000 R CNN
F 2 "SFUSat-cap:C_0805" H 12150 5050 50  0001 C CNN
F 3 "" H 12150 5050 50  0001 C CNN
F 4 "General Purpose" H 12150 5050 50  0001 C CNN "Applications"
F 5 "10µF" H 12150 5050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 12150 5050 50  0001 C CNN "Categories"
F 7 "-" H 12150 5050 50  0001 C CNN "Features"
F 8 "-" H 12150 5050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 12150 5050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 12150 5050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 12150 5050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 12150 5050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 12150 5050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 12150 5050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 12150 5050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 12150 5050 50  0001 C CNN "Package / Case"
F 17 "Active" H 12150 5050 50  0001 C CNN "Part Status"
F 18 "-" H 12150 5050 50  0001 C CNN "Ratings"
F 19 "GRM" H 12150 5050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 12150 5050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 12150 5050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 12150 5050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 12150 5050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 12150 5050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 12150 5050 50  0001 C CNN "Tolerance"
F 26 "25V" H 12150 5050 50  0001 C CNN "Voltage - Rated"
	1    12150 5050
	-1   0    0    1   
$EndComp
Connection ~ 12150 4900
Wire Wire Line
	12150 4900 12400 4900
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C26
U 1 1 5C0A7F12
P 12400 5050
F 0 "C26" H 12285 5004 50  0000 R CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 12285 5095 50  0000 R CNN
F 2 "SFUSat-cap:C_0805" H 12400 5050 50  0001 C CNN
F 3 "" H 12400 5050 50  0001 C CNN
F 4 "General Purpose" H 12400 5050 50  0001 C CNN "Applications"
F 5 "10µF" H 12400 5050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 12400 5050 50  0001 C CNN "Categories"
F 7 "-" H 12400 5050 50  0001 C CNN "Features"
F 8 "-" H 12400 5050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 12400 5050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 12400 5050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 12400 5050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 12400 5050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 12400 5050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 12400 5050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 12400 5050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 12400 5050 50  0001 C CNN "Package / Case"
F 17 "Active" H 12400 5050 50  0001 C CNN "Part Status"
F 18 "-" H 12400 5050 50  0001 C CNN "Ratings"
F 19 "GRM" H 12400 5050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 12400 5050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 12400 5050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 12400 5050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 12400 5050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 12400 5050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 12400 5050 50  0001 C CNN "Tolerance"
F 26 "25V" H 12400 5050 50  0001 C CNN "Voltage - Rated"
	1    12400 5050
	-1   0    0    1   
$EndComp
Connection ~ 12400 4900
Wire Wire Line
	12400 4900 12650 4900
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C27
U 1 1 5C0A7F96
P 12650 5050
F 0 "C27" H 12535 5004 50  0000 R CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 12535 5095 50  0000 R CNN
F 2 "SFUSat-cap:C_0805" H 12650 5050 50  0001 C CNN
F 3 "" H 12650 5050 50  0001 C CNN
F 4 "General Purpose" H 12650 5050 50  0001 C CNN "Applications"
F 5 "10µF" H 12650 5050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 12650 5050 50  0001 C CNN "Categories"
F 7 "-" H 12650 5050 50  0001 C CNN "Features"
F 8 "-" H 12650 5050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 12650 5050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 12650 5050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 12650 5050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 12650 5050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 12650 5050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 12650 5050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 12650 5050 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 12650 5050 50  0001 C CNN "Package / Case"
F 17 "Active" H 12650 5050 50  0001 C CNN "Part Status"
F 18 "-" H 12650 5050 50  0001 C CNN "Ratings"
F 19 "GRM" H 12650 5050 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 12650 5050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 12650 5050 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 12650 5050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 12650 5050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 12650 5050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 12650 5050 50  0001 C CNN "Tolerance"
F 26 "25V" H 12650 5050 50  0001 C CNN "Voltage - Rated"
	1    12650 5050
	-1   0    0    1   
$EndComp
Wire Wire Line
	13550 5100 13450 5100
Wire Wire Line
	13450 5100 13450 5200
Wire Wire Line
	13450 5200 12650 5200
Connection ~ 13550 5100
Wire Wire Line
	12650 5200 12400 5200
Connection ~ 12650 5200
Wire Wire Line
	12400 5200 12150 5200
Connection ~ 12400 5200
Wire Wire Line
	12150 5200 11900 5200
Connection ~ 12150 5200
Wire Wire Line
	11900 5200 11650 5200
Connection ~ 11900 5200
Wire Wire Line
	11650 5200 11400 5200
Connection ~ 11650 5200
$Comp
L SFUSat-res:R_10m0_5%_0.25W_0805 R2
U 1 1 5C0E0E5B
P 6200 5250
F 0 "R2" H 6200 5457 50  0000 C CNN
F 1 "R_10m0_5%_0.25W_0805" H 6200 5366 50  0000 C CNN
F 2 "SFUSat-res:R_0805" H 6200 5250 50  0001 C CNN
F 3 "" H 6200 5250 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6200 5250 50  0001 C CNN "Categories"
F 5 "Thin Film" H 6200 5250 50  0001 C CNN "Composition"
F 6 "-" H 6200 5250 50  0001 C CNN "Failure Rate"
F 7 "Current Sense" H 6200 5250 50  0001 C CNN "Features"
F 8 "0.028\" (0.70mm)" H 6200 5250 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6200 5250 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Susumu" H 6200 5250 50  0001 C CNN "Manufacturer 1"
F 11 "RL1220T-R010-J" H 6200 5250 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "10 Weeks" H 6200 5250 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 6200 5250 50  0001 C CNN "Number of Terminations"
F 14 "-55�C ~ 125�C" H 6200 5250 50  0001 C CNN "Operating Temperature"
F 15 "0805 (2012 Metric)" H 6200 5250 50  0001 C CNN "Package / Case"
F 16 "Active" H 6200 5250 50  0001 C CNN "Part Status"
F 17 "0.25W, 1/4W" H 6200 5250 50  0001 C CNN "Power (Watts)"
F 18 "10 mOhms" H 6200 5250 50  0001 C CNN "Resistance"
F 19 "RL" H 6200 5250 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 6200 5250 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 6200 5250 50  0001 C CNN "Supplier 1"
F 22 "0805" H 6200 5250 50  0001 C CNN "Supplier Device Package"
F 23 "RL12T.010JCT-ND" H 6200 5250 50  0001 C CNN "Supplier Part Number 1"
F 24 "0/ +350ppm/�C" H 6200 5250 50  0001 C CNN "Temperature Coefficient"
F 25 "�5%" H 6200 5250 50  0001 C CNN "Tolerance"
	1    6200 5250
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-res:R_4R99_1%_0.1W_0603 R3
U 1 1 5C0E1348
P 6350 5400
F 0 "R3" V 6304 5470 50  0000 L CNN
F 1 "R_4R99_1%_0.1W_0603" V 6395 5470 50  0000 L CNN
F 2 "SFUSat-res:R_0603" H 6350 5400 50  0001 C CNN
F 3 "" H 6350 5400 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6350 5400 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6350 5400 50  0001 C CNN "Composition"
F 6 "-" H 6350 5400 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6350 5400 50  0001 C CNN "Features"
F 8 "0.020\" (0.50mm)" H 6350 5400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6350 5400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6350 5400 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW06034R99FKEA" H 6350 5400 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6350 5400 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6350 5400 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 6350 5400 50  0001 C CNN "Package / Case"
F 15 "Active" H 6350 5400 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 6350 5400 50  0001 C CNN "Power (Watts)"
F 17 "4.99 Ohms" H 6350 5400 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6350 5400 50  0001 C CNN "Series"
F 19 "0.063\" L x 0.033\" W (1.60mm x 0.85mm)" H 6350 5400 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6350 5400 50  0001 C CNN "Supplier 1"
F 21 "0603" H 6350 5400 50  0001 C CNN "Supplier Device Package"
F 22 "541-4.99HHCT-ND" H 6350 5400 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6350 5400 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6350 5400 50  0001 C CNN "Tolerance"
	1    6350 5400
	0    1    1    0   
$EndComp
$Comp
L SFUSat-res:R_4R99_1%_0.1W_0603 R1
U 1 1 5C0EAF48
P 6050 5400
F 0 "R1" V 6004 5470 50  0000 L CNN
F 1 "R_4R99_1%_0.1W_0603" V 6095 5470 50  0000 L CNN
F 2 "SFUSat-res:R_0603" H 6050 5400 50  0001 C CNN
F 3 "" H 6050 5400 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6050 5400 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6050 5400 50  0001 C CNN "Composition"
F 6 "-" H 6050 5400 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6050 5400 50  0001 C CNN "Features"
F 8 "0.020\" (0.50mm)" H 6050 5400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6050 5400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6050 5400 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW06034R99FKEA" H 6050 5400 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6050 5400 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6050 5400 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 6050 5400 50  0001 C CNN "Package / Case"
F 15 "Active" H 6050 5400 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 6050 5400 50  0001 C CNN "Power (Watts)"
F 17 "4.99 Ohms" H 6050 5400 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6050 5400 50  0001 C CNN "Series"
F 19 "0.063\" L x 0.033\" W (1.60mm x 0.85mm)" H 6050 5400 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6050 5400 50  0001 C CNN "Supplier 1"
F 21 "0603" H 6050 5400 50  0001 C CNN "Supplier Device Package"
F 22 "541-4.99HHCT-ND" H 6050 5400 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6050 5400 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6050 5400 50  0001 C CNN "Tolerance"
	1    6050 5400
	0    1    1    0   
$EndComp
Connection ~ 6350 5250
Wire Wire Line
	6050 5550 6050 7400
$Comp
L SFUSat-cap:C_33n0_10%_25V_X7R_0603 C6
U 1 1 5C0FEA9B
P 6200 6100
F 0 "C6" H 6315 6146 50  0000 L CNN
F 1 "C_33n0_10%_25V_X7R_0603" H 6315 6055 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 6200 6100 50  0001 C CNN
F 3 "" H 6200 6100 50  0001 C CNN
F 4 "General Purpose" H 6200 6100 50  0001 C CNN "Applications"
F 5 "0.033µF" H 6200 6100 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 6200 6100 50  0001 C CNN "Categories"
F 7 "-" H 6200 6100 50  0001 C CNN "Features"
F 8 "-" H 6200 6100 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6200 6100 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 6200 6100 50  0001 C CNN "Lead Spacing"
F 11 "-" H 6200 6100 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 6200 6100 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E333KA01D" H 6200 6100 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 6200 6100 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 6200 6100 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 6200 6100 50  0001 C CNN "Package / Case"
F 17 "Active" H 6200 6100 50  0001 C CNN "Part Status"
F 18 "-" H 6200 6100 50  0001 C CNN "Ratings"
F 19 "GRM" H 6200 6100 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 6200 6100 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 6200 6100 50  0001 C CNN "Supplier 1"
F 22 "490-1521-1-ND" H 6200 6100 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 6200 6100 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 6200 6100 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 6200 6100 50  0001 C CNN "Tolerance"
F 26 "25V" H 6200 6100 50  0001 C CNN "Voltage - Rated"
	1    6200 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5550 6350 5750
Wire Wire Line
	6200 5950 6200 5750
Wire Wire Line
	6200 5750 6350 5750
Connection ~ 6350 5750
Wire Wire Line
	6350 5750 6350 7200
$Comp
L SFUSat-res:R_1R0_5%_0.125W_0805 R4
U 1 1 5C1091E4
P 6500 5400
F 0 "R4" V 6454 5470 50  0000 L CNN
F 1 "R_1R0_5%_0.125W_0805" V 6545 5470 50  0000 L CNN
F 2 "SFUSat-res:R_0805" H 6500 5400 50  0001 C CNN
F 3 "" H 6500 5400 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6500 5400 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6500 5400 50  0001 C CNN "Composition"
F 6 "-" H 6500 5400 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6500 5400 50  0001 C CNN "Features"
F 8 "0.020\" (0.50mm)" H 6500 5400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6500 5400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6500 5400 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW08051R00JNEA" H 6500 5400 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6500 5400 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6500 5400 50  0001 C CNN "Operating Temperature"
F 14 "0805 (2012 Metric)" H 6500 5400 50  0001 C CNN "Package / Case"
F 15 "Active" H 6500 5400 50  0001 C CNN "Part Status"
F 16 "0.125W, 1/8W" H 6500 5400 50  0001 C CNN "Power (Watts)"
F 17 "1 Ohms" H 6500 5400 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6500 5400 50  0001 C CNN "Series"
F 19 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 6500 5400 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6500 5400 50  0001 C CNN "Supplier 1"
F 21 "0805" H 6500 5400 50  0001 C CNN "Supplier Device Package"
F 22 "541-1.0ACT-ND" H 6500 5400 50  0001 C CNN "Supplier Part Number 1"
F 23 "�200ppm/�C" H 6500 5400 50  0001 C CNN "Temperature Coefficient"
F 24 "�5%" H 6500 5400 50  0001 C CNN "Tolerance"
	1    6500 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 5250 6500 5250
Connection ~ 6500 5250
Wire Wire Line
	6500 5700 6500 5550
Connection ~ 6500 5700
$Comp
L SFUSat-cap:C_470n0_10%_16V_X7R_0805 C7
U 1 1 5C11D702
P 6600 5850
F 0 "C7" H 6715 5896 50  0000 L CNN
F 1 "C_470n0_10%_16V_X7R_0805" H 6715 5805 50  0000 L CNN
F 2 "SFUSat-cap:C_0805" H 6600 5850 50  0001 C CNN
F 3 "" H 6600 5850 50  0001 C CNN
F 4 "General Purpose" H 6600 5850 50  0001 C CNN "Applications"
F 5 "0.47µF" H 6600 5850 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 6600 5850 50  0001 C CNN "Categories"
F 7 "-" H 6600 5850 50  0001 C CNN "Failure Rate"
F 8 "Low ESL" H 6600 5850 50  0001 C CNN "Features"
F 9 "-" H 6600 5850 50  0001 C CNN "Height - Seated (Max)"
F 10 "Lead free / RoHS Compliant" H 6600 5850 50  0001 C CNN "Lead Free Status / RoHS Status"
F 11 "-" H 6600 5850 50  0001 C CNN "Lead Spacing"
F 12 "-" H 6600 5850 50  0001 C CNN "Lead Style"
F 13 "KEMET" H 6600 5850 50  0001 C CNN "Manufacturer 1"
F 14 "C0805C474K4RACTU" H 6600 5850 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "19 Weeks" H 6600 5850 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 16 "Surface Mount, MLCC" H 6600 5850 50  0001 C CNN "Mounting Type"
F 17 "-55°C ~ 125°C" H 6600 5850 50  0001 C CNN "Operating Temperature"
F 18 "0805 (2012 Metric)" H 6600 5850 50  0001 C CNN "Package / Case"
F 19 "Active" H 6600 5850 50  0001 C CNN "Part Status"
F 20 "-" H 6600 5850 50  0001 C CNN "Ratings"
F 21 "C" H 6600 5850 50  0001 C CNN "Series"
F 22 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 6600 5850 50  0001 C CNN "Size / Dimension"
F 23 "Digi-Key" H 6600 5850 50  0001 C CNN "Supplier 1"
F 24 "399-8099-1-ND" H 6600 5850 50  0001 C CNN "Supplier Part Number 1"
F 25 "X7R" H 6600 5850 50  0001 C CNN "Temperature Coefficient"
F 26 "0.055\" (1.40mm)" H 6600 5850 50  0001 C CNN "Thickness (Max)"
F 27 "±10%" H 6600 5850 50  0001 C CNN "Tolerance"
F 28 "16V" H 6600 5850 50  0001 C CNN "Voltage - Rated"
	1    6600 5850
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-cap:C_10n0_10%_25V_X7R_0603 C8
U 1 1 5C11DDC1
P 6750 5400
F 0 "C8" H 6865 5446 50  0000 L CNN
F 1 "C_10n0_10%_25V_X7R_0603" H 6865 5355 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 6750 5400 50  0001 C CNN
F 3 "" H 6750 5400 50  0001 C CNN
F 4 "General Purpose" H 6750 5400 50  0001 C CNN "Applications"
F 5 "10000pF" H 6750 5400 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 6750 5400 50  0001 C CNN "Categories"
F 7 "-" H 6750 5400 50  0001 C CNN "Features"
F 8 "-" H 6750 5400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6750 5400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 6750 5400 50  0001 C CNN "Lead Spacing"
F 11 "-" H 6750 5400 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 6750 5400 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E103KA01D" H 6750 5400 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 6750 5400 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 6750 5400 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 6750 5400 50  0001 C CNN "Package / Case"
F 17 "Active" H 6750 5400 50  0001 C CNN "Part Status"
F 18 "-" H 6750 5400 50  0001 C CNN "Ratings"
F 19 "GRM" H 6750 5400 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 6750 5400 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 6750 5400 50  0001 C CNN "Supplier 1"
F 22 "490-1520-1-ND" H 6750 5400 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 6750 5400 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 6750 5400 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 6750 5400 50  0001 C CNN "Tolerance"
F 26 "25V" H 6750 5400 50  0001 C CNN "Voltage - Rated"
	1    6750 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5250 6750 5250
$Comp
L SFUSat-cap:C_33n0_10%_25V_X7R_0603 C5
U 1 1 5C11E587
P 6050 7550
F 0 "C5" H 6165 7596 50  0000 L CNN
F 1 "C_33n0_10%_25V_X7R_0603" H 5100 7450 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 6050 7550 50  0001 C CNN
F 3 "" H 6050 7550 50  0001 C CNN
F 4 "General Purpose" H 6050 7550 50  0001 C CNN "Applications"
F 5 "0.033µF" H 6050 7550 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 6050 7550 50  0001 C CNN "Categories"
F 7 "-" H 6050 7550 50  0001 C CNN "Features"
F 8 "-" H 6050 7550 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6050 7550 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 6050 7550 50  0001 C CNN "Lead Spacing"
F 11 "-" H 6050 7550 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 6050 7550 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E333KA01D" H 6050 7550 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 6050 7550 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 6050 7550 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 6050 7550 50  0001 C CNN "Package / Case"
F 17 "Active" H 6050 7550 50  0001 C CNN "Part Status"
F 18 "-" H 6050 7550 50  0001 C CNN "Ratings"
F 19 "GRM" H 6050 7550 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 6050 7550 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 6050 7550 50  0001 C CNN "Supplier 1"
F 22 "490-1521-1-ND" H 6050 7550 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 6050 7550 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 6050 7550 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 6050 7550 50  0001 C CNN "Tolerance"
F 26 "25V" H 6050 7550 50  0001 C CNN "Voltage - Rated"
	1    6050 7550
	1    0    0    -1  
$EndComp
Connection ~ 6050 7400
$Comp
L SFUSat-ind:L_2u2_20%_8.5A_8030 L1
U 1 1 5C11ED90
P 8350 5250
F 0 "L1" H 8350 5440 50  0000 C CNN
F 1 "L_2u2_20%_8.5A_8030" H 8350 5349 50  0000 C CNN
F 2 "SFUSat-ind:L_8030" H 8350 5250 50  0001 C CNN
F 3 "" H 8350 5250 50  0001 C CNN
F 4 "Inductors, Coils, Chokes - Fixed Inductors" H 8350 5250 50  0001 C CNN "Categories"
F 5 "15.5A" H 8350 5250 50  0001 C CNN "Current - Saturation"
F 6 "8.5A" H 8350 5250 50  0001 C CNN "Current Rating"
F 7 "20.3 mOhm Max" H 8350 5250 50  0001 C CNN "DC Resistance (DCR)"
F 8 "24MHz" H 8350 5250 50  0001 C CNN "Frequency - Self Resonant"
F 9 "100kHz" H 8350 5250 50  0001 C CNN "Frequency - Test"
F 10 "0.118\" (3.00mm)" H 8350 5250 50  0001 C CNN "Height - Seated (Max)"
F 11 "2.2µH" H 8350 5250 50  0001 C CNN "Inductance"
F 12 "Lead free / RoHS Compliant" H 8350 5250 50  0001 C CNN "Lead Free Status / RoHS Status"
F 13 "Wurth Electronics Inc." H 8350 5250 50  0001 C CNN "Manufacturer 1"
F 14 "74437356022" H 8350 5250 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "12 Weeks" H 8350 5250 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 16 "Powdered Iron" H 8350 5250 50  0001 C CNN "Material - Core"
F 17 "Surface Mount" H 8350 5250 50  0001 C CNN "Mounting Type"
F 18 "-40°C ~ 125°C" H 8350 5250 50  0001 C CNN "Operating Temperature"
F 19 "Nonstandard" H 8350 5250 50  0001 C CNN "Package / Case"
F 20 "Active" H 8350 5250 50  0001 C CNN "Part Status"
F 21 "-" H 8350 5250 50  0001 C CNN "Q @ Freq"
F 22 "-" H 8350 5250 50  0001 C CNN "Ratings"
F 23 "WE-LHMI" H 8350 5250 50  0001 C CNN "Series"
F 24 "Shielded" H 8350 5250 50  0001 C CNN "Shielding"
F 25 "0.362\" L x 0.335\" W (9.20mm x 8.50mm)" H 8350 5250 50  0001 C CNN "Size / Dimension"
F 26 "Digi-Key" H 8350 5250 50  0001 C CNN "Supplier 1"
F 27 "8030" H 8350 5250 50  0001 C CNN "Supplier Device Package"
F 28 "732-7201-1-ND" H 8350 5250 50  0001 C CNN "Supplier Part Number 1"
F 29 "±20%" H 8350 5250 50  0001 C CNN "Tolerance"
F 30 "Molded" H 8350 5250 50  0001 C CNN "Type"
	1    8350 5250
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-cap:C_47n0_10%_25V_X7R_0603 C12
U 1 1 5C1339C8
P 8050 5400
F 0 "C12" H 8165 5446 50  0000 L CNN
F 1 "C_47n0_10%_25V_X7R_0603" H 8165 5355 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 8050 5400 50  0001 C CNN
F 3 "" H 8050 5400 50  0001 C CNN
F 4 "General Purpose" H 8050 5400 50  0001 C CNN "Applications"
F 5 "0.047µF" H 8050 5400 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 8050 5400 50  0001 C CNN "Categories"
F 7 "-" H 8050 5400 50  0001 C CNN "Features"
F 8 "-" H 8050 5400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 8050 5400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 8050 5400 50  0001 C CNN "Lead Spacing"
F 11 "-" H 8050 5400 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 8050 5400 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E473KA01D" H 8050 5400 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 8050 5400 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 8050 5400 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 8050 5400 50  0001 C CNN "Package / Case"
F 17 "Active" H 8050 5400 50  0001 C CNN "Part Status"
F 18 "-" H 8050 5400 50  0001 C CNN "Ratings"
F 19 "GRM" H 8050 5400 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 8050 5400 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 8050 5400 50  0001 C CNN "Supplier 1"
F 22 "490-1522-1-ND" H 8050 5400 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 8050 5400 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 8050 5400 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 8050 5400 50  0001 C CNN "Tolerance"
F 26 "25V" H 8050 5400 50  0001 C CNN "Voltage - Rated"
	1    8050 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5250 8650 5250
Wire Wire Line
	7400 5250 8050 5250
Connection ~ 8050 5250
Wire Wire Line
	8050 5250 8200 5250
$Comp
L SFUSat-cap:C_47n0_10%_25V_X7R_0603 C13
U 1 1 5C133C68
P 8650 5400
F 0 "C13" H 8765 5446 50  0000 L CNN
F 1 "C_47n0_10%_25V_X7R_0603" H 8765 5355 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 8650 5400 50  0001 C CNN
F 3 "" H 8650 5400 50  0001 C CNN
F 4 "General Purpose" H 8650 5400 50  0001 C CNN "Applications"
F 5 "0.047µF" H 8650 5400 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 8650 5400 50  0001 C CNN "Categories"
F 7 "-" H 8650 5400 50  0001 C CNN "Features"
F 8 "-" H 8650 5400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 8650 5400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 8650 5400 50  0001 C CNN "Lead Spacing"
F 11 "-" H 8650 5400 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 8650 5400 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E473KA01D" H 8650 5400 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 8650 5400 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 8650 5400 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 8650 5400 50  0001 C CNN "Package / Case"
F 17 "Active" H 8650 5400 50  0001 C CNN "Part Status"
F 18 "-" H 8650 5400 50  0001 C CNN "Ratings"
F 19 "GRM" H 8650 5400 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 8650 5400 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 8650 5400 50  0001 C CNN "Supplier 1"
F 22 "490-1522-1-ND" H 8650 5400 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 8650 5400 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 8650 5400 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 8650 5400 50  0001 C CNN "Tolerance"
F 26 "25V" H 8650 5400 50  0001 C CNN "Voltage - Rated"
	1    8650 5400
	1    0    0    -1  
$EndComp
Connection ~ 8650 5250
$Comp
L SFUSat-res:R_10m0_5%_0.25W_0805 R21
U 1 1 5C134196
P 11750 5500
F 0 "R21" H 11750 5707 50  0000 C CNN
F 1 "R_10m0_5%_0.25W_0805" H 11750 5616 50  0000 C CNN
F 2 "SFUSat-res:R_0805" H 11750 5500 50  0001 C CNN
F 3 "" H 11750 5500 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 11750 5500 50  0001 C CNN "Categories"
F 5 "Thin Film" H 11750 5500 50  0001 C CNN "Composition"
F 6 "-" H 11750 5500 50  0001 C CNN "Failure Rate"
F 7 "Current Sense" H 11750 5500 50  0001 C CNN "Features"
F 8 "0.028\" (0.70mm)" H 11750 5500 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11750 5500 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Susumu" H 11750 5500 50  0001 C CNN "Manufacturer 1"
F 11 "RL1220T-R010-J" H 11750 5500 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "10 Weeks" H 11750 5500 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 11750 5500 50  0001 C CNN "Number of Terminations"
F 14 "-55�C ~ 125�C" H 11750 5500 50  0001 C CNN "Operating Temperature"
F 15 "0805 (2012 Metric)" H 11750 5500 50  0001 C CNN "Package / Case"
F 16 "Active" H 11750 5500 50  0001 C CNN "Part Status"
F 17 "0.25W, 1/4W" H 11750 5500 50  0001 C CNN "Power (Watts)"
F 18 "10 mOhms" H 11750 5500 50  0001 C CNN "Resistance"
F 19 "RL" H 11750 5500 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 11750 5500 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 11750 5500 50  0001 C CNN "Supplier 1"
F 22 "0805" H 11750 5500 50  0001 C CNN "Supplier Device Package"
F 23 "RL12T.010JCT-ND" H 11750 5500 50  0001 C CNN "Supplier Part Number 1"
F 24 "0/ +350ppm/�C" H 11750 5500 50  0001 C CNN "Temperature Coefficient"
F 25 "�5%" H 11750 5500 50  0001 C CNN "Tolerance"
	1    11750 5500
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-res:R_10R0_1%_0.1W_0603 R20
U 1 1 5C134518
P 11500 5650
F 0 "R20" V 11454 5720 50  0000 L CNN
F 1 "R_10R0_1%_0.1W_0603" V 11545 5720 50  0000 L CNN
F 2 "SFUSat-res:R_0603" H 11500 5650 50  0001 C CNN
F 3 "" H 11500 5650 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 11500 5650 50  0001 C CNN "Categories"
F 5 "Thick Film" H 11500 5650 50  0001 C CNN "Composition"
F 6 "-" H 11500 5650 50  0001 C CNN "Failure Rate"
F 7 "-" H 11500 5650 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 11500 5650 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11500 5650 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 11500 5650 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW060310R0FKEAC" H 11500 5650 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 11500 5650 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 11500 5650 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 11500 5650 50  0001 C CNN "Package / Case"
F 15 "Active" H 11500 5650 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 11500 5650 50  0001 C CNN "Power (Watts)"
F 17 "10 Ohms" H 11500 5650 50  0001 C CNN "Resistance"
F 18 "CRCW-C" H 11500 5650 50  0001 C CNN "Series"
F 19 "Digi-Key" H 11500 5650 50  0001 C CNN "Supplier 1"
F 20 "0603" H 11500 5650 50  0001 C CNN "Supplier Device Package"
F 21 "541-3952-1-ND" H 11500 5650 50  0001 C CNN "Supplier Part Number 1"
F 22 "�100ppm/�C" H 11500 5650 50  0001 C CNN "Temperature Coefficient"
F 23 "�1%" H 11500 5650 50  0001 C CNN "Tolerance"
	1    11500 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	11900 5500 12000 5500
Connection ~ 11500 5500
Wire Wire Line
	11500 5500 11600 5500
$Comp
L SFUSat-res:R_10R0_1%_0.1W_0603 R22
U 1 1 5C13E3A1
P 12000 5650
F 0 "R22" V 11954 5720 50  0000 L CNN
F 1 "R_10R0_1%_0.1W_0603" V 12045 5720 50  0000 L CNN
F 2 "SFUSat-res:R_0603" H 12000 5650 50  0001 C CNN
F 3 "" H 12000 5650 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 12000 5650 50  0001 C CNN "Categories"
F 5 "Thick Film" H 12000 5650 50  0001 C CNN "Composition"
F 6 "-" H 12000 5650 50  0001 C CNN "Failure Rate"
F 7 "-" H 12000 5650 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 12000 5650 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 12000 5650 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 12000 5650 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW060310R0FKEAC" H 12000 5650 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 12000 5650 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 12000 5650 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 12000 5650 50  0001 C CNN "Package / Case"
F 15 "Active" H 12000 5650 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 12000 5650 50  0001 C CNN "Power (Watts)"
F 17 "10 Ohms" H 12000 5650 50  0001 C CNN "Resistance"
F 18 "CRCW-C" H 12000 5650 50  0001 C CNN "Series"
F 19 "Digi-Key" H 12000 5650 50  0001 C CNN "Supplier 1"
F 20 "0603" H 12000 5650 50  0001 C CNN "Supplier Device Package"
F 21 "541-3952-1-ND" H 12000 5650 50  0001 C CNN "Supplier Part Number 1"
F 22 "�100ppm/�C" H 12000 5650 50  0001 C CNN "Temperature Coefficient"
F 23 "�1%" H 12000 5650 50  0001 C CNN "Tolerance"
	1    12000 5650
	0    1    1    0   
$EndComp
$Comp
L SFUSat-cap:C_100n0_10%_25V_X7R_0603 C23
U 1 1 5C13E514
P 11750 6050
F 0 "C23" V 11498 6050 50  0000 C CNN
F 1 "C_100n0_10%_25V_X7R_0603" V 11589 6050 50  0000 C CNN
F 2 "SFUSat-cap:C_0603" H 11750 6050 50  0001 C CNN
F 3 "" H 11750 6050 50  0001 C CNN
F 4 "General Purpose" H 11750 6050 50  0001 C CNN "Applications"
F 5 "0.1µF" H 11750 6050 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 11750 6050 50  0001 C CNN "Categories"
F 7 "-" H 11750 6050 50  0001 C CNN "Features"
F 8 "-" H 11750 6050 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11750 6050 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 11750 6050 50  0001 C CNN "Lead Spacing"
F 11 "-" H 11750 6050 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 11750 6050 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E104KA01D" H 11750 6050 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 11750 6050 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 11750 6050 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 11750 6050 50  0001 C CNN "Package / Case"
F 17 "Active" H 11750 6050 50  0001 C CNN "Part Status"
F 18 "-" H 11750 6050 50  0001 C CNN "Ratings"
F 19 "GRM" H 11750 6050 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 11750 6050 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 11750 6050 50  0001 C CNN "Supplier 1"
F 22 "490-1524-1-ND" H 11750 6050 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 11750 6050 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 11750 6050 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 11750 6050 50  0001 C CNN "Tolerance"
F 26 "25V" H 11750 6050 50  0001 C CNN "Voltage - Rated"
	1    11750 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	11900 6050 12000 6050
Connection ~ 12000 6050
Wire Wire Line
	12000 6050 12000 5800
Wire Wire Line
	11600 6050 11500 6050
Connection ~ 11500 6050
Wire Wire Line
	11500 6050 11500 5800
$Comp
L SFUSat-res:R_10R0_1%_0.1W_0603 R10
U 1 1 5C15CD2E
P 7000 7600
F 0 "R10" H 7000 7807 50  0000 C CNN
F 1 "R_10R0_1%_0.1W_0603" H 7000 7716 50  0000 C CNN
F 2 "SFUSat-res:R_0603" H 7000 7600 50  0001 C CNN
F 3 "" H 7000 7600 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 7000 7600 50  0001 C CNN "Categories"
F 5 "Thick Film" H 7000 7600 50  0001 C CNN "Composition"
F 6 "-" H 7000 7600 50  0001 C CNN "Failure Rate"
F 7 "-" H 7000 7600 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 7000 7600 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7000 7600 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 7000 7600 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW060310R0FKEAC" H 7000 7600 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 7000 7600 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 7000 7600 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 7000 7600 50  0001 C CNN "Package / Case"
F 15 "Active" H 7000 7600 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 7000 7600 50  0001 C CNN "Power (Watts)"
F 17 "10 Ohms" H 7000 7600 50  0001 C CNN "Resistance"
F 18 "CRCW-C" H 7000 7600 50  0001 C CNN "Series"
F 19 "Digi-Key" H 7000 7600 50  0001 C CNN "Supplier 1"
F 20 "0603" H 7000 7600 50  0001 C CNN "Supplier Device Package"
F 21 "541-3952-1-ND" H 7000 7600 50  0001 C CNN "Supplier Part Number 1"
F 22 "�100ppm/�C" H 7000 7600 50  0001 C CNN "Temperature Coefficient"
F 23 "�1%" H 7000 7600 50  0001 C CNN "Tolerance"
	1    7000 7600
	1    0    0    -1  
$EndComp
Connection ~ 7150 7600
$Comp
L SFUSat-cap:C_1u0_10%_25V_X7R_0603 C10
U 1 1 5C15D01C
P 7150 7800
F 0 "C10" H 7265 7846 50  0000 L CNN
F 1 "C_1u0_10%_25V_X7R_0603" H 7265 7755 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 7150 7800 50  0001 C CNN
F 3 "" H 7150 7800 50  0001 C CNN
F 4 "General Purpose" H 7150 7800 50  0001 C CNN "Applications"
F 5 "1µF" H 7150 7800 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 7150 7800 50  0001 C CNN "Categories"
F 7 "-" H 7150 7800 50  0001 C CNN "Features"
F 8 "-" H 7150 7800 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7150 7800 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 7150 7800 50  0001 C CNN "Lead Spacing"
F 11 "-" H 7150 7800 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 7150 7800 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R71E105KA12D" H 7150 7800 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 7150 7800 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 7150 7800 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 7150 7800 50  0001 C CNN "Package / Case"
F 17 "Active" H 7150 7800 50  0001 C CNN "Part Status"
F 18 "-" H 7150 7800 50  0001 C CNN "Ratings"
F 19 "GRM" H 7150 7800 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 7150 7800 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 7150 7800 50  0001 C CNN "Supplier 1"
F 22 "490-5307-1-ND" H 7150 7800 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 7150 7800 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 7150 7800 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 7150 7800 50  0001 C CNN "Tolerance"
F 26 "25V" H 7150 7800 50  0001 C CNN "Voltage - Rated"
	1    7150 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 8000 7150 7950
$Comp
L SFUSat-res:R_383k0_1%_0.1W_0603 R13
U 1 1 5C1674DE
P 7450 7750
F 0 "R13" V 7404 7820 50  0000 L CNN
F 1 "R_383k0_1%_0.1W_0603" V 7495 7820 50  0000 L CNN
F 2 "SFUSat-res:R_0603" H 7450 7750 50  0001 C CNN
F 3 "" H 7450 7750 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 7450 7750 50  0001 C CNN "Categories"
F 5 "Thick Film" H 7450 7750 50  0001 C CNN "Composition"
F 6 "-" H 7450 7750 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 7450 7750 50  0001 C CNN "Features"
F 8 "0.020\" (0.50mm)" H 7450 7750 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7450 7750 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 7450 7750 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW0603383KFKEA" H 7450 7750 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 7450 7750 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 7450 7750 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 7450 7750 50  0001 C CNN "Package / Case"
F 15 "Active" H 7450 7750 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 7450 7750 50  0001 C CNN "Power (Watts)"
F 17 "383 kOhms" H 7450 7750 50  0001 C CNN "Resistance"
F 18 "CRCW" H 7450 7750 50  0001 C CNN "Series"
F 19 "0.063\" L x 0.033\" W (1.60mm x 0.85mm)" H 7450 7750 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 7450 7750 50  0001 C CNN "Supplier 1"
F 21 "0603" H 7450 7750 50  0001 C CNN "Supplier Device Package"
F 22 "541-383KHCT-ND" H 7450 7750 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 7450 7750 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 7450 7750 50  0001 C CNN "Tolerance"
	1    7450 7750
	0    1    1    0   
$EndComp
$Comp
L SFUSat-res:R_220k0_1%_0.1W_0603 R11
U 1 1 5C1719E1
P 7300 8000
F 0 "R11" H 7300 8207 50  0000 C CNN
F 1 "R_220k0_1%_0.1W_0603" H 7300 8116 50  0000 C CNN
F 2 "SFUSat-res:R_0603" H 7300 8000 50  0001 C CNN
F 3 "" H 7300 8000 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 7300 8000 50  0001 C CNN "Categories"
F 5 "Thick Film" H 7300 8000 50  0001 C CNN "Composition"
F 6 "-" H 7300 8000 50  0001 C CNN "Failure Rate"
F 7 "Moisture Resistant" H 7300 8000 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 7300 8000 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7300 8000 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Yageo" H 7300 8000 50  0001 C CNN "Manufacturer 1"
F 11 "RC0603FR-07220KL" H 7300 8000 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "16 Weeks" H 7300 8000 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 7300 8000 50  0001 C CNN "Number of Terminations"
F 14 "-55�C ~ 155�C" H 7300 8000 50  0001 C CNN "Operating Temperature"
F 15 "0603 (1608 Metric)" H 7300 8000 50  0001 C CNN "Package / Case"
F 16 "Active" H 7300 8000 50  0001 C CNN "Part Status"
F 17 "0.1W, 1/10W" H 7300 8000 50  0001 C CNN "Power (Watts)"
F 18 "220 kOhms" H 7300 8000 50  0001 C CNN "Resistance"
F 19 "RC" H 7300 8000 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 7300 8000 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 7300 8000 50  0001 C CNN "Supplier 1"
F 22 "0603" H 7300 8000 50  0001 C CNN "Supplier Device Package"
F 23 "311-220KHRCT-ND" H 7300 8000 50  0001 C CNN "Supplier Part Number 1"
F 24 "�100ppm/�C" H 7300 8000 50  0001 C CNN "Temperature Coefficient"
F 25 "�1%" H 7300 8000 50  0001 C CNN "Tolerance"
	1    7300 8000
	1    0    0    -1  
$EndComp
Connection ~ 7150 8000
Connection ~ 7450 8000
$Comp
L SFUSat-res:R_40k2_1%_0.063W_0402 R12
U 1 1 5C171CB8
P 7300 8150
F 0 "R12" H 7300 8357 50  0000 C CNN
F 1 "R_40k2_1%_0.063W_0402" H 7300 8266 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 7300 8150 50  0001 C CNN
F 3 "" H 7300 8150 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 7300 8150 50  0001 C CNN "Categories"
F 5 "Thick Film" H 7300 8150 50  0001 C CNN "Composition"
F 6 "-" H 7300 8150 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 7300 8150 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 7300 8150 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7300 8150 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 7300 8150 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040240K2FKED" H 7300 8150 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 7300 8150 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 7300 8150 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 7300 8150 50  0001 C CNN "Package / Case"
F 15 "Active" H 7300 8150 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 7300 8150 50  0001 C CNN "Power (Watts)"
F 17 "40.2 kOhms" H 7300 8150 50  0001 C CNN "Resistance"
F 18 "CRCW" H 7300 8150 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 7300 8150 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 7300 8150 50  0001 C CNN "Supplier 1"
F 21 "0402" H 7300 8150 50  0001 C CNN "Supplier Device Package"
F 22 "541-40.2KLCT-ND" H 7300 8150 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 7300 8150 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 7300 8150 50  0001 C CNN "Tolerance"
	1    7300 8150
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-cap:C_33p0_5%_50V_NP0_0402 C11
U 1 1 5C171F8A
P 7200 8300
F 0 "C11" V 6948 8300 50  0000 C CNN
F 1 "C_33p0_5%_50V_NP0_0402" V 7039 8300 50  0000 C CNN
F 2 "SFUSat-cap:C_0402" H 7200 8300 50  0001 C CNN
F 3 "" H 7200 8300 50  0001 C CNN
F 4 "General Purpose" H 7200 8300 50  0001 C CNN "Applications"
F 5 "33pF" H 7200 8300 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 7200 8300 50  0001 C CNN "Categories"
F 7 "-" H 7200 8300 50  0001 C CNN "Features"
F 8 "-" H 7200 8300 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7200 8300 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 7200 8300 50  0001 C CNN "Lead Spacing"
F 11 "-" H 7200 8300 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 7200 8300 50  0001 C CNN "Manufacturer 1"
F 13 "GRM1555C1H330JA01D" H 7200 8300 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 7200 8300 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 7200 8300 50  0001 C CNN "Operating Temperature"
F 16 "0402 (1005 Metric)" H 7200 8300 50  0001 C CNN "Package / Case"
F 17 "Active" H 7200 8300 50  0001 C CNN "Part Status"
F 18 "-" H 7200 8300 50  0001 C CNN "Ratings"
F 19 "GRM" H 7200 8300 50  0001 C CNN "Series"
F 20 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 7200 8300 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 7200 8300 50  0001 C CNN "Supplier 1"
F 22 "490-5936-1-ND" H 7200 8300 50  0001 C CNN "Supplier Part Number 1"
F 23 "C0G, NP0" H 7200 8300 50  0001 C CNN "Temperature Coefficient"
F 24 "0.022\" (0.55mm)" H 7200 8300 50  0001 C CNN "Thickness (Max)"
F 25 "±5%" H 7200 8300 50  0001 C CNN "Tolerance"
F 26 "50V" H 7200 8300 50  0001 C CNN "Voltage - Rated"
	1    7200 8300
	0    1    1    0   
$EndComp
Wire Wire Line
	7350 8300 7450 8300
Connection ~ 7450 8300
$Comp
L SFUSat-cap:C_1n8_10%_50V_X7R_0402 C9
U 1 1 5C1867AB
P 7000 8150
F 0 "C9" V 6748 8150 50  0000 C CNN
F 1 "C_1n8_10%_50V_X7R_0402" V 6839 8150 50  0000 C CNN
F 2 "SFUSat-cap:C_0402" H 7000 8150 50  0001 C CNN
F 3 "" H 7000 8150 50  0001 C CNN
F 4 "General Purpose" H 7000 8150 50  0001 C CNN "Applications"
F 5 "1800pF" H 7000 8150 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 7000 8150 50  0001 C CNN "Categories"
F 7 "-" H 7000 8150 50  0001 C CNN "Features"
F 8 "-" H 7000 8150 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 7000 8150 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 7000 8150 50  0001 C CNN "Lead Spacing"
F 11 "-" H 7000 8150 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 7000 8150 50  0001 C CNN "Manufacturer 1"
F 13 "GRM155R71H182KA01D" H 7000 8150 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 7000 8150 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 7000 8150 50  0001 C CNN "Operating Temperature"
F 16 "0402 (1005 Metric)" H 7000 8150 50  0001 C CNN "Package / Case"
F 17 "Active" H 7000 8150 50  0001 C CNN "Part Status"
F 18 "-" H 7000 8150 50  0001 C CNN "Ratings"
F 19 "GRM" H 7000 8150 50  0001 C CNN "Series"
F 20 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 7000 8150 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 7000 8150 50  0001 C CNN "Supplier 1"
F 22 "490-3246-1-ND" H 7000 8150 50  0001 C CNN "Supplier Part Number 1"
F 23 "X7R" H 7000 8150 50  0001 C CNN "Temperature Coefficient"
F 24 "0.022\" (0.55mm)" H 7000 8150 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 7000 8150 50  0001 C CNN "Tolerance"
F 26 "50V" H 7000 8150 50  0001 C CNN "Voltage - Rated"
	1    7000 8150
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 8300 6850 8150
Wire Wire Line
	6850 8300 7050 8300
Connection ~ 6850 8150
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R5
U 1 1 5C19B4E3
P 6550 8600
F 0 "R5" H 6550 8807 50  0000 C CNN
F 1 "R_10k0_1%_0.063W_0402" H 6550 8716 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 6550 8600 50  0001 C CNN
F 3 "" H 6550 8600 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6550 8600 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6550 8600 50  0001 C CNN "Composition"
F 6 "-" H 6550 8600 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6550 8600 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 6550 8600 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6550 8600 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6550 8600 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 6550 8600 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6550 8600 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6550 8600 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 6550 8600 50  0001 C CNN "Package / Case"
F 15 "Active" H 6550 8600 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 6550 8600 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 6550 8600 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6550 8600 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 6550 8600 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6550 8600 50  0001 C CNN "Supplier 1"
F 21 "0402" H 6550 8600 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 6550 8600 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6550 8600 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6550 8600 50  0001 C CNN "Tolerance"
	1    6550 8600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 8600 6400 8800
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R6
U 1 1 5C19B829
P 6550 8800
F 0 "R6" H 6550 9007 50  0000 C CNN
F 1 "R_10k0_1%_0.063W_0402" H 6550 8916 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 6550 8800 50  0001 C CNN
F 3 "" H 6550 8800 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6550 8800 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6550 8800 50  0001 C CNN "Composition"
F 6 "-" H 6550 8800 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6550 8800 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 6550 8800 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6550 8800 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6550 8800 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 6550 8800 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6550 8800 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6550 8800 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 6550 8800 50  0001 C CNN "Package / Case"
F 15 "Active" H 6550 8800 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 6550 8800 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 6550 8800 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6550 8800 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 6550 8800 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6550 8800 50  0001 C CNN "Supplier 1"
F 21 "0402" H 6550 8800 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 6550 8800 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6550 8800 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6550 8800 50  0001 C CNN "Tolerance"
	1    6550 8800
	1    0    0    -1  
$EndComp
Connection ~ 6400 8800
Wire Wire Line
	6400 8800 6400 9000
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R7
U 1 1 5C19B8DB
P 6550 9000
F 0 "R7" H 6550 9207 50  0000 C CNN
F 1 "R_10k0_1%_0.063W_0402" H 6550 9116 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 6550 9000 50  0001 C CNN
F 3 "" H 6550 9000 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6550 9000 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6550 9000 50  0001 C CNN "Composition"
F 6 "-" H 6550 9000 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6550 9000 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 6550 9000 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6550 9000 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6550 9000 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 6550 9000 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6550 9000 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6550 9000 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 6550 9000 50  0001 C CNN "Package / Case"
F 15 "Active" H 6550 9000 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 6550 9000 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 6550 9000 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6550 9000 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 6550 9000 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6550 9000 50  0001 C CNN "Supplier 1"
F 21 "0402" H 6550 9000 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 6550 9000 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6550 9000 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6550 9000 50  0001 C CNN "Tolerance"
	1    6550 9000
	1    0    0    -1  
$EndComp
Connection ~ 6400 9000
Wire Wire Line
	6400 9000 6400 9200
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R8
U 1 1 5C19B98D
P 6550 9200
F 0 "R8" H 6550 9407 50  0000 C CNN
F 1 "R_10k0_1%_0.063W_0402" H 6550 9316 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 6550 9200 50  0001 C CNN
F 3 "" H 6550 9200 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6550 9200 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6550 9200 50  0001 C CNN "Composition"
F 6 "-" H 6550 9200 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6550 9200 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 6550 9200 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6550 9200 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6550 9200 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 6550 9200 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6550 9200 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6550 9200 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 6550 9200 50  0001 C CNN "Package / Case"
F 15 "Active" H 6550 9200 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 6550 9200 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 6550 9200 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6550 9200 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 6550 9200 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6550 9200 50  0001 C CNN "Supplier 1"
F 21 "0402" H 6550 9200 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 6550 9200 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6550 9200 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6550 9200 50  0001 C CNN "Tolerance"
	1    6550 9200
	1    0    0    -1  
$EndComp
Connection ~ 6400 9200
Wire Wire Line
	6400 9200 6400 9600
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R9
U 1 1 5C19BA43
P 6550 9600
F 0 "R9" H 6550 9807 50  0000 C CNN
F 1 "R_10k0_1%_0.063W_0402" H 6550 9716 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 6550 9600 50  0001 C CNN
F 3 "" H 6550 9600 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6550 9600 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6550 9600 50  0001 C CNN "Composition"
F 6 "-" H 6550 9600 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 6550 9600 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 6550 9600 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6550 9600 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 6550 9600 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 6550 9600 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 6550 9600 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 6550 9600 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 6550 9600 50  0001 C CNN "Package / Case"
F 15 "Active" H 6550 9600 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 6550 9600 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 6550 9600 50  0001 C CNN "Resistance"
F 18 "CRCW" H 6550 9600 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 6550 9600 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 6550 9600 50  0001 C CNN "Supplier 1"
F 21 "0402" H 6550 9600 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 6550 9600 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 6550 9600 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 6550 9600 50  0001 C CNN "Tolerance"
	1    6550 9600
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R19
U 1 1 5C19BE17
P 11450 8250
F 0 "R19" V 11404 8320 50  0000 L CNN
F 1 "R_10k0_1%_0.063W_0402" V 11495 8320 50  0000 L CNN
F 2 "SFUSat-res:R_0402" H 11450 8250 50  0001 C CNN
F 3 "" H 11450 8250 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 11450 8250 50  0001 C CNN "Categories"
F 5 "Thick Film" H 11450 8250 50  0001 C CNN "Composition"
F 6 "-" H 11450 8250 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 11450 8250 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 11450 8250 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 11450 8250 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 11450 8250 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 11450 8250 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 11450 8250 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 11450 8250 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 11450 8250 50  0001 C CNN "Package / Case"
F 15 "Active" H 11450 8250 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 11450 8250 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 11450 8250 50  0001 C CNN "Resistance"
F 18 "CRCW" H 11450 8250 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 11450 8250 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 11450 8250 50  0001 C CNN "Supplier 1"
F 21 "0402" H 11450 8250 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 11450 8250 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 11450 8250 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 11450 8250 50  0001 C CNN "Tolerance"
	1    11450 8250
	0    1    1    0   
$EndComp
Wire Wire Line
	11050 8400 11450 8400
Connection ~ 11450 8400
Wire Wire Line
	11450 8400 12850 8400
Wire Wire Line
	9250 8100 9250 8250
$Comp
L SFUSat-res:R_10k0_1%_0.063W_0402 R15
U 1 1 5C1BB33A
P 9400 8250
F 0 "R15" H 9400 8043 50  0000 C CNN
F 1 "R_10k0_1%_0.063W_0402" H 9400 8134 50  0000 C CNN
F 2 "SFUSat-res:R_0402" H 9400 8250 50  0001 C CNN
F 3 "" H 9400 8250 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 9400 8250 50  0001 C CNN "Categories"
F 5 "Thick Film" H 9400 8250 50  0001 C CNN "Composition"
F 6 "-" H 9400 8250 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 9400 8250 50  0001 C CNN "Features"
F 8 "0.016\" (0.40mm)" H 9400 8250 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9400 8250 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 9400 8250 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW040210K0FKED" H 9400 8250 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 9400 8250 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 9400 8250 50  0001 C CNN "Operating Temperature"
F 14 "0402 (1005 Metric)" H 9400 8250 50  0001 C CNN "Package / Case"
F 15 "Active" H 9400 8250 50  0001 C CNN "Part Status"
F 16 "0.063W, 1/16W" H 9400 8250 50  0001 C CNN "Power (Watts)"
F 17 "10 kOhms" H 9400 8250 50  0001 C CNN "Resistance"
F 18 "CRCW" H 9400 8250 50  0001 C CNN "Series"
F 19 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 9400 8250 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 9400 8250 50  0001 C CNN "Supplier 1"
F 21 "0402" H 9400 8250 50  0001 C CNN "Supplier Device Package"
F 22 "541-10.0KLCT-ND" H 9400 8250 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 9400 8250 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 9400 8250 50  0001 C CNN "Tolerance"
	1    9400 8250
	-1   0    0    1   
$EndComp
Connection ~ 9250 8250
Wire Wire Line
	9250 8250 9250 8300
$Comp
L SFUSat-cap:C_15p0_5%_50V_NP0_0603 C16
U 1 1 5C1C5ADF
P 9600 8100
F 0 "C16" V 9348 8100 50  0000 C CNN
F 1 "C_15p0_5%_50V_NP0_0603" V 9439 8100 50  0000 C CNN
F 2 "SFUSat-cap:C_0603" H 9600 8100 50  0001 C CNN
F 3 "" H 9600 8100 50  0001 C CNN
F 4 "General Purpose" H 9600 8100 50  0001 C CNN "Applications"
F 5 "15pF" H 9600 8100 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 9600 8100 50  0001 C CNN "Categories"
F 7 "-" H 9600 8100 50  0001 C CNN "Failure Rate"
F 8 "Low ESL" H 9600 8100 50  0001 C CNN "Features"
F 9 "-" H 9600 8100 50  0001 C CNN "Height - Seated (Max)"
F 10 "Lead free / RoHS Compliant" H 9600 8100 50  0001 C CNN "Lead Free Status / RoHS Status"
F 11 "-" H 9600 8100 50  0001 C CNN "Lead Spacing"
F 12 "-" H 9600 8100 50  0001 C CNN "Lead Style"
F 13 "KEMET" H 9600 8100 50  0001 C CNN "Manufacturer 1"
F 14 "C0603C150J5GACTU" H 9600 8100 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "9 Weeks" H 9600 8100 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 16 "Surface Mount, MLCC" H 9600 8100 50  0001 C CNN "Mounting Type"
F 17 "-55°C ~ 125°C" H 9600 8100 50  0001 C CNN "Operating Temperature"
F 18 "0603 (1608 Metric)" H 9600 8100 50  0001 C CNN "Package / Case"
F 19 "Active" H 9600 8100 50  0001 C CNN "Part Status"
F 20 "-" H 9600 8100 50  0001 C CNN "Ratings"
F 21 "C" H 9600 8100 50  0001 C CNN "Series"
F 22 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 9600 8100 50  0001 C CNN "Size / Dimension"
F 23 "Digi-Key" H 9600 8100 50  0001 C CNN "Supplier 1"
F 24 "399-1051-1-ND" H 9600 8100 50  0001 C CNN "Supplier Part Number 1"
F 25 "C0G, NP0" H 9600 8100 50  0001 C CNN "Temperature Coefficient"
F 26 "0.034\" (0.87mm)" H 9600 8100 50  0001 C CNN "Thickness (Max)"
F 27 "±5%" H 9600 8100 50  0001 C CNN "Tolerance"
F 28 "50V" H 9600 8100 50  0001 C CNN "Voltage - Rated"
	1    9600 8100
	0    1    1    0   
$EndComp
$Comp
L SFUSat-cap:C_680p0_5%_25V_NP0_0402 C17
U 1 1 5C1D021C
P 9700 8250
F 0 "C17" V 9448 8250 50  0000 C CNN
F 1 "C_680p0_5%_25V_NP0_0402" V 9539 8250 50  0000 C CNN
F 2 "SFUSat-cap:C_0402" H 9700 8250 50  0001 C CNN
F 3 "" H 9700 8250 50  0001 C CNN
F 4 "General Purpose" H 9700 8250 50  0001 C CNN "Applications"
F 5 "680pF" H 9700 8250 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 9700 8250 50  0001 C CNN "Categories"
F 7 "-" H 9700 8250 50  0001 C CNN "Features"
F 8 "-" H 9700 8250 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9700 8250 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 9700 8250 50  0001 C CNN "Lead Spacing"
F 11 "-" H 9700 8250 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 9700 8250 50  0001 C CNN "Manufacturer 1"
F 13 "GRM1555C1E681JA01D" H 9700 8250 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 9700 8250 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 125°C" H 9700 8250 50  0001 C CNN "Operating Temperature"
F 16 "0402 (1005 Metric)" H 9700 8250 50  0001 C CNN "Package / Case"
F 17 "Active" H 9700 8250 50  0001 C CNN "Part Status"
F 18 "-" H 9700 8250 50  0001 C CNN "Ratings"
F 19 "GRM" H 9700 8250 50  0001 C CNN "Series"
F 20 "0.039\" L x 0.020\" W (1.00mm x 0.50mm)" H 9700 8250 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 9700 8250 50  0001 C CNN "Supplier 1"
F 22 "490-6182-1-ND" H 9700 8250 50  0001 C CNN "Supplier Part Number 1"
F 23 "C0G, NP0" H 9700 8250 50  0001 C CNN "Temperature Coefficient"
F 24 "0.022\" (0.55mm)" H 9700 8250 50  0001 C CNN "Thickness (Max)"
F 25 "±5%" H 9700 8250 50  0001 C CNN "Tolerance"
F 26 "25V" H 9700 8250 50  0001 C CNN "Voltage - Rated"
	1    9700 8250
	0    1    1    0   
$EndComp
Wire Wire Line
	9850 8250 9900 8250
Wire Wire Line
	9900 8250 9900 8100
Wire Wire Line
	9900 8100 9750 8100
Connection ~ 9900 8250
$Comp
L SFUSat-res:R_220k0_1%_0.1W_0603 R14
U 1 1 5C1EEDB8
P 9250 8550
F 0 "R14" V 9204 8620 50  0000 L CNN
F 1 "R_220k0_1%_0.1W_0603" V 9295 8620 50  0000 L CNN
F 2 "SFUSat-res:R_0603" H 9250 8550 50  0001 C CNN
F 3 "" H 9250 8550 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 9250 8550 50  0001 C CNN "Categories"
F 5 "Thick Film" H 9250 8550 50  0001 C CNN "Composition"
F 6 "-" H 9250 8550 50  0001 C CNN "Failure Rate"
F 7 "Moisture Resistant" H 9250 8550 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 9250 8550 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9250 8550 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Yageo" H 9250 8550 50  0001 C CNN "Manufacturer 1"
F 11 "RC0603FR-07220KL" H 9250 8550 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "16 Weeks" H 9250 8550 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 9250 8550 50  0001 C CNN "Number of Terminations"
F 14 "-55�C ~ 155�C" H 9250 8550 50  0001 C CNN "Operating Temperature"
F 15 "0603 (1608 Metric)" H 9250 8550 50  0001 C CNN "Package / Case"
F 16 "Active" H 9250 8550 50  0001 C CNN "Part Status"
F 17 "0.1W, 1/10W" H 9250 8550 50  0001 C CNN "Power (Watts)"
F 18 "220 kOhms" H 9250 8550 50  0001 C CNN "Resistance"
F 19 "RC" H 9250 8550 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 9250 8550 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 9250 8550 50  0001 C CNN "Supplier 1"
F 22 "0603" H 9250 8550 50  0001 C CNN "Supplier Device Package"
F 23 "311-220KHRCT-ND" H 9250 8550 50  0001 C CNN "Supplier Part Number 1"
F 24 "�100ppm/�C" H 9250 8550 50  0001 C CNN "Temperature Coefficient"
F 25 "�1%" H 9250 8550 50  0001 C CNN "Tolerance"
	1    9250 8550
	0    1    1    0   
$EndComp
Connection ~ 9250 8700
$Comp
L SFUSat-res:R_357k0_0.1%_0.125W_0805 R17
U 1 1 5C1F963A
P 9700 8700
F 0 "R17" H 9700 8907 50  0000 C CNN
F 1 "R_357k0_0.1%_0.125W_0805" H 9700 8816 50  0000 C CNN
F 2 "SFUSat-res:R_0805" H 9700 8700 50  0001 C CNN
F 3 "" H 9700 8700 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 9700 8700 50  0001 C CNN "Categories"
F 5 "Thin Film" H 9700 8700 50  0001 C CNN "Composition"
F 6 "-" H 9700 8700 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 9700 8700 50  0001 C CNN "Features"
F 8 "0.024\" (0.60mm)" H 9700 8700 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9700 8700 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Panasonic Electronic Components" H 9700 8700 50  0001 C CNN "Manufacturer 1"
F 11 "ERA-6AEB3573V" H 9700 8700 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "22 Weeks" H 9700 8700 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 9700 8700 50  0001 C CNN "Number of Terminations"
F 14 "-55�C ~ 155�C" H 9700 8700 50  0001 C CNN "Operating Temperature"
F 15 "0805 (2012 Metric)" H 9700 8700 50  0001 C CNN "Package / Case"
F 16 "Active" H 9700 8700 50  0001 C CNN "Part Status"
F 17 "0.125W, 1/8W" H 9700 8700 50  0001 C CNN "Power (Watts)"
F 18 "357 kOhms" H 9700 8700 50  0001 C CNN "Resistance"
F 19 "ERA-6A" H 9700 8700 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 9700 8700 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 9700 8700 50  0001 C CNN "Supplier 1"
F 22 "0805" H 9700 8700 50  0001 C CNN "Supplier Device Package"
F 23 "P357KDACT-ND" H 9700 8700 50  0001 C CNN "Supplier Part Number 1"
F 24 "�25ppm/�C" H 9700 8700 50  0001 C CNN "Temperature Coefficient"
F 25 "�0.1%" H 9700 8700 50  0001 C CNN "Tolerance"
	1    9700 8700
	1    0    0    -1  
$EndComp
Connection ~ 9550 8700
$Comp
L SFUSat-res:R_137k0_1%_0.1W_0603 R18
U 1 1 5C1F99E0
P 9850 9000
F 0 "R18" H 9850 9207 50  0000 C CNN
F 1 "R_137k0_1%_0.1W_0603" H 9850 9116 50  0000 C CNN
F 2 "SFUSat-res:R_0603" H 9850 9000 50  0001 C CNN
F 3 "" H 9850 9000 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 9850 9000 50  0001 C CNN "Categories"
F 5 "Thick Film" H 9850 9000 50  0001 C CNN "Composition"
F 6 "-" H 9850 9000 50  0001 C CNN "Failure Rate"
F 7 "Moisture Resistant" H 9850 9000 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 9850 9000 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9850 9000 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Yageo" H 9850 9000 50  0001 C CNN "Manufacturer 1"
F 11 "RC0603FR-07137KL" H 9850 9000 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "16 Weeks" H 9850 9000 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 9850 9000 50  0001 C CNN "Number of Terminations"
F 14 "-55�C ~ 155�C" H 9850 9000 50  0001 C CNN "Operating Temperature"
F 15 "0603 (1608 Metric)" H 9850 9000 50  0001 C CNN "Package / Case"
F 16 "Active" H 9850 9000 50  0001 C CNN "Part Status"
F 17 "0.1W, 1/10W" H 9850 9000 50  0001 C CNN "Power (Watts)"
F 18 "137 kOhms" H 9850 9000 50  0001 C CNN "Resistance"
F 19 "RC" H 9850 9000 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 9850 9000 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 9850 9000 50  0001 C CNN "Supplier 1"
F 22 "0603" H 9850 9000 50  0001 C CNN "Supplier Device Package"
F 23 "311-137KHRCT-ND" H 9850 9000 50  0001 C CNN "Supplier Part Number 1"
F 24 "�100ppm/�C" H 9850 9000 50  0001 C CNN "Temperature Coefficient"
F 25 "�1%" H 9850 9000 50  0001 C CNN "Tolerance"
	1    9850 9000
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-cap:C_100p0_5%_50V_NP0_0603 C18
U 1 1 5C1F9D0F
P 9700 9150
F 0 "C18" H 9585 9104 50  0000 R CNN
F 1 "C_100p0_5%_50V_NP0_0603" H 9585 9195 50  0000 R CNN
F 2 "SFUSat-cap:C_0603" H 9700 9150 50  0001 C CNN
F 3 "" H 9700 9150 50  0001 C CNN
F 4 "Automotive" H 9700 9150 50  0001 C CNN "Applications"
F 5 "100pF" H 9700 9150 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 9700 9150 50  0001 C CNN "Categories"
F 7 "-" H 9700 9150 50  0001 C CNN "Failure Rate"
F 8 "-" H 9700 9150 50  0001 C CNN "Features"
F 9 "-" H 9700 9150 50  0001 C CNN "Height - Seated (Max)"
F 10 "Lead free / RoHS Compliant" H 9700 9150 50  0001 C CNN "Lead Free Status / RoHS Status"
F 11 "-" H 9700 9150 50  0001 C CNN "Lead Spacing"
F 12 "-" H 9700 9150 50  0001 C CNN "Lead Style"
F 13 "KEMET" H 9700 9150 50  0001 C CNN "Manufacturer 1"
F 14 "C0603C101J5GACAUTO" H 9700 9150 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "13 Weeks" H 9700 9150 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 16 "Surface Mount, MLCC" H 9700 9150 50  0001 C CNN "Mounting Type"
F 17 "-55°C ~ 125°C" H 9700 9150 50  0001 C CNN "Operating Temperature"
F 18 "0603 (1608 Metric)" H 9700 9150 50  0001 C CNN "Package / Case"
F 19 "Active" H 9700 9150 50  0001 C CNN "Part Status"
F 20 "AEC-Q200" H 9700 9150 50  0001 C CNN "Ratings"
F 21 "C" H 9700 9150 50  0001 C CNN "Series"
F 22 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 9700 9150 50  0001 C CNN "Size / Dimension"
F 23 "Digi-Key" H 9700 9150 50  0001 C CNN "Supplier 1"
F 24 "399-6842-1-ND" H 9700 9150 50  0001 C CNN "Supplier Part Number 1"
F 25 "C0G, NP0" H 9700 9150 50  0001 C CNN "Temperature Coefficient"
F 26 "0.034\" (0.87mm)" H 9700 9150 50  0001 C CNN "Thickness (Max)"
F 27 "±5%" H 9700 9150 50  0001 C CNN "Tolerance"
F 28 "50V" H 9700 9150 50  0001 C CNN "Voltage - Rated"
	1    9700 9150
	-1   0    0    1   
$EndComp
Wire Wire Line
	9700 9300 9900 9300
Wire Wire Line
	10000 9000 10000 9300
Wire Wire Line
	10000 9300 9900 9300
Connection ~ 9900 9300
Wire Wire Line
	9250 9000 9700 9000
Connection ~ 9700 9000
$Comp
L SFUSat-cap:C_100p0_5%_50V_NP0_0603 C15
U 1 1 5C22D266
P 9400 9200
F 0 "C15" V 9652 9200 50  0000 C CNN
F 1 "C_100p0_5%_50V_NP0_0603" V 9561 9200 50  0000 C CNN
F 2 "SFUSat-cap:C_0603" H 9400 9200 50  0001 C CNN
F 3 "" H 9400 9200 50  0001 C CNN
F 4 "Automotive" H 9400 9200 50  0001 C CNN "Applications"
F 5 "100pF" H 9400 9200 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 9400 9200 50  0001 C CNN "Categories"
F 7 "-" H 9400 9200 50  0001 C CNN "Failure Rate"
F 8 "-" H 9400 9200 50  0001 C CNN "Features"
F 9 "-" H 9400 9200 50  0001 C CNN "Height - Seated (Max)"
F 10 "Lead free / RoHS Compliant" H 9400 9200 50  0001 C CNN "Lead Free Status / RoHS Status"
F 11 "-" H 9400 9200 50  0001 C CNN "Lead Spacing"
F 12 "-" H 9400 9200 50  0001 C CNN "Lead Style"
F 13 "KEMET" H 9400 9200 50  0001 C CNN "Manufacturer 1"
F 14 "C0603C101J5GACAUTO" H 9400 9200 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "13 Weeks" H 9400 9200 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 16 "Surface Mount, MLCC" H 9400 9200 50  0001 C CNN "Mounting Type"
F 17 "-55°C ~ 125°C" H 9400 9200 50  0001 C CNN "Operating Temperature"
F 18 "0603 (1608 Metric)" H 9400 9200 50  0001 C CNN "Package / Case"
F 19 "Active" H 9400 9200 50  0001 C CNN "Part Status"
F 20 "AEC-Q200" H 9400 9200 50  0001 C CNN "Ratings"
F 21 "C" H 9400 9200 50  0001 C CNN "Series"
F 22 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 9400 9200 50  0001 C CNN "Size / Dimension"
F 23 "Digi-Key" H 9400 9200 50  0001 C CNN "Supplier 1"
F 24 "399-6842-1-ND" H 9400 9200 50  0001 C CNN "Supplier Part Number 1"
F 25 "C0G, NP0" H 9400 9200 50  0001 C CNN "Temperature Coefficient"
F 26 "0.034\" (0.87mm)" H 9400 9200 50  0001 C CNN "Thickness (Max)"
F 27 "±5%" H 9400 9200 50  0001 C CNN "Tolerance"
F 28 "50V" H 9400 9200 50  0001 C CNN "Voltage - Rated"
	1    9400 9200
	0    -1   -1   0   
$EndComp
$Comp
L SFUSat-res:R_30k1_1%_0.1W_0603 R16
U 1 1 5C241CF7
P 9400 9400
F 0 "R16" H 9400 9607 50  0000 C CNN
F 1 "R_30k1_1%_0.1W_0603" H 9400 9516 50  0000 C CNN
F 2 "SFUSat-res:R_0603" H 9400 9400 50  0001 C CNN
F 3 "" H 9400 9400 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 9400 9400 50  0001 C CNN "Categories"
F 5 "Thick Film" H 9400 9400 50  0001 C CNN "Composition"
F 6 "-" H 9400 9400 50  0001 C CNN "Failure Rate"
F 7 "Automotive AEC-Q200" H 9400 9400 50  0001 C CNN "Features"
F 8 "0.020\" (0.50mm)" H 9400 9400 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9400 9400 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Vishay Dale" H 9400 9400 50  0001 C CNN "Manufacturer 1"
F 11 "CRCW060330K1FKEA" H 9400 9400 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "2" H 9400 9400 50  0001 C CNN "Number of Terminations"
F 13 "-55�C ~ 155�C" H 9400 9400 50  0001 C CNN "Operating Temperature"
F 14 "0603 (1608 Metric)" H 9400 9400 50  0001 C CNN "Package / Case"
F 15 "Active" H 9400 9400 50  0001 C CNN "Part Status"
F 16 "0.1W, 1/10W" H 9400 9400 50  0001 C CNN "Power (Watts)"
F 17 "30.1 kOhms" H 9400 9400 50  0001 C CNN "Resistance"
F 18 "CRCW" H 9400 9400 50  0001 C CNN "Series"
F 19 "0.063\" L x 0.033\" W (1.60mm x 0.85mm)" H 9400 9400 50  0001 C CNN "Size / Dimension"
F 20 "Digi-Key" H 9400 9400 50  0001 C CNN "Supplier 1"
F 21 "0603" H 9400 9400 50  0001 C CNN "Supplier Device Package"
F 22 "541-30.1KHCT-ND" H 9400 9400 50  0001 C CNN "Supplier Part Number 1"
F 23 "�100ppm/�C" H 9400 9400 50  0001 C CNN "Temperature Coefficient"
F 24 "�1%" H 9400 9400 50  0001 C CNN "Tolerance"
	1    9400 9400
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-cap:C_2u2_10%_35V_X5R_0603 C19
U 1 1 5C242247
P 10250 8850
F 0 "C19" H 10365 8896 50  0000 L CNN
F 1 "C_2u2_10%_35V_X5R_0603" H 10365 8805 50  0000 L CNN
F 2 "SFUSat-cap:C_0603" H 10250 8850 50  0001 C CNN
F 3 "" H 10250 8850 50  0001 C CNN
F 4 "General Purpose" H 10250 8850 50  0001 C CNN "Applications"
F 5 "2.2µF" H 10250 8850 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 10250 8850 50  0001 C CNN "Categories"
F 7 "-" H 10250 8850 50  0001 C CNN "Features"
F 8 "-" H 10250 8850 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 10250 8850 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 10250 8850 50  0001 C CNN "Lead Spacing"
F 11 "-" H 10250 8850 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 10250 8850 50  0001 C CNN "Manufacturer 1"
F 13 "GRM188R6YA225KA12D" H 10250 8850 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 10250 8850 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 10250 8850 50  0001 C CNN "Operating Temperature"
F 16 "0603 (1608 Metric)" H 10250 8850 50  0001 C CNN "Package / Case"
F 17 "Active" H 10250 8850 50  0001 C CNN "Part Status"
F 18 "-" H 10250 8850 50  0001 C CNN "Ratings"
F 19 "GRM" H 10250 8850 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 10250 8850 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 10250 8850 50  0001 C CNN "Supplier 1"
F 22 "490-7204-1-ND" H 10250 8850 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 10250 8850 50  0001 C CNN "Temperature Coefficient"
F 24 "0.035\" (0.90mm)" H 10250 8850 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 10250 8850 50  0001 C CNN "Tolerance"
F 26 "35V" H 10250 8850 50  0001 C CNN "Voltage - Rated"
	1    10250 8850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 9600 10250 9000
$Comp
L SFUSat-cap:C_10u0_10%_25V_X5R_0805 C14
U 1 1 5C24D285
P 8950 4100
F 0 "C14" H 9065 4146 50  0000 L CNN
F 1 "C_10u0_10%_25V_X5R_0805" H 9065 4055 50  0000 L CNN
F 2 "SFUSat-cap:C_0805" H 8950 4100 50  0001 C CNN
F 3 "" H 8950 4100 50  0001 C CNN
F 4 "General Purpose" H 8950 4100 50  0001 C CNN "Applications"
F 5 "10µF" H 8950 4100 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 8950 4100 50  0001 C CNN "Categories"
F 7 "-" H 8950 4100 50  0001 C CNN "Features"
F 8 "-" H 8950 4100 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 8950 4100 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 8950 4100 50  0001 C CNN "Lead Spacing"
F 11 "-" H 8950 4100 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 8950 4100 50  0001 C CNN "Manufacturer 1"
F 13 "GRM21BR61E106KA73L" H 8950 4100 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 8950 4100 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 8950 4100 50  0001 C CNN "Operating Temperature"
F 16 "0805 (2012 Metric)" H 8950 4100 50  0001 C CNN "Package / Case"
F 17 "Active" H 8950 4100 50  0001 C CNN "Part Status"
F 18 "-" H 8950 4100 50  0001 C CNN "Ratings"
F 19 "GRM" H 8950 4100 50  0001 C CNN "Series"
F 20 "0.079\" L x 0.049\" W (2.00mm x 1.25mm)" H 8950 4100 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 8950 4100 50  0001 C CNN "Supplier 1"
F 22 "490-5523-1-ND" H 8950 4100 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 8950 4100 50  0001 C CNN "Temperature Coefficient"
F 24 "0.053\" (1.35mm)" H 8950 4100 50  0001 C CNN "Thickness (Max)"
F 25 "±10%" H 8950 4100 50  0001 C CNN "Tolerance"
F 26 "25V" H 8950 4100 50  0001 C CNN "Voltage - Rated"
	1    8950 4100
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-cap:C_22u0_20%_10V_X5R_1206 C20
U 1 1 5C24D468
P 10650 4100
F 0 "C20" H 10765 4146 50  0000 L CNN
F 1 "C_22u0_20%_10V_X5R_1206" H 10765 4055 50  0000 L CNN
F 2 "SFUSat-cap:C_1206" H 10650 4100 50  0001 C CNN
F 3 "" H 10650 4100 50  0001 C CNN
F 4 "General Purpose" H 10650 4100 50  0001 C CNN "Applications"
F 5 "22µF" H 10650 4100 50  0001 C CNN "Capacitance"
F 6 "Capacitors - Ceramic Capacitors" H 10650 4100 50  0001 C CNN "Categories"
F 7 "-" H 10650 4100 50  0001 C CNN "Features"
F 8 "-" H 10650 4100 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 10650 4100 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "-" H 10650 4100 50  0001 C CNN "Lead Spacing"
F 11 "-" H 10650 4100 50  0001 C CNN "Lead Style"
F 12 "Murata Electronics North America" H 10650 4100 50  0001 C CNN "Manufacturer 1"
F 13 "GRM31CR61A226ME19L" H 10650 4100 50  0001 C CNN "Manufacturer Part Number 1"
F 14 "Surface Mount, MLCC" H 10650 4100 50  0001 C CNN "Mounting Type"
F 15 "-55°C ~ 85°C" H 10650 4100 50  0001 C CNN "Operating Temperature"
F 16 "1206 (3216 Metric)" H 10650 4100 50  0001 C CNN "Package / Case"
F 17 "Discontinued at Digi-Key" H 10650 4100 50  0001 C CNN "Part Status"
F 18 "-" H 10650 4100 50  0001 C CNN "Ratings"
F 19 "GRM" H 10650 4100 50  0001 C CNN "Series"
F 20 "0.126\" L x 0.063\" W (3.20mm x 1.60mm)" H 10650 4100 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 10650 4100 50  0001 C CNN "Supplier 1"
F 22 "490-4738-1-ND" H 10650 4100 50  0001 C CNN "Supplier Part Number 1"
F 23 "X5R" H 10650 4100 50  0001 C CNN "Temperature Coefficient"
F 24 "0.071\" (1.80mm)" H 10650 4100 50  0001 C CNN "Thickness (Max)"
F 25 "±20%" H 10650 4100 50  0001 C CNN "Tolerance"
F 26 "10V" H 10650 4100 50  0001 C CNN "Voltage - Rated"
	1    10650 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4450 8950 4250
Wire Wire Line
	10650 4250 10650 4450
$Comp
L SFUSat:CSD17551Q3A Q1
U 1 1 5C2A59DD
P 6300 4550
F 0 "Q1" H 6300 4975 50  0000 C CNN
F 1 "CSD17551Q3A" H 6300 4884 50  0000 C CNN
F 2 "SFUSat:8-PowerWDFN" H 6300 4550 50  0001 C CNN
F 3 "" H 6300 4550 50  0001 C CNN
F 4 "Discrete Semiconductor Products - Transistors - FETs, MOSFETs - Single" H 6300 4550 50  0001 C CNN "Categories"
F 5 "12A (Tc)" H 6300 4550 50  0001 C CNN "Current - Continuous Drain (Id) @ 25°C"
F 6 "30V" H 6300 4550 50  0001 C CNN "Drain to Source Voltage (Vdss)"
F 7 "4.5V, 0V" H 6300 4550 50  0001 C CNN "Drive Voltage (Max Rds On,  Min Rds On)"
F 8 "-" H 6300 4550 50  0001 C CNN "FET Feature"
F 9 "N-Channel" H 6300 4550 50  0001 C CNN "FET Type"
F 10 "7.8nC @ 4.5V" H 6300 4550 50  0001 C CNN "Gate Charge (Qg) (Max) @ Vgs"
F 11 "1370pF @ 15V" H 6300 4550 50  0001 C CNN "Input Capacitance (Ciss) (Max) @ Vds"
F 12 "Lead free / RoHS Compliant" H 6300 4550 50  0001 C CNN "Lead Free Status / RoHS Status"
F 13 "Texas Instruments" H 6300 4550 50  0001 C CNN "Manufacturer 1"
F 14 "CSD17551Q3A" H 6300 4550 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "Surface Mount" H 6300 4550 50  0001 C CNN "Mounting Type"
F 16 "-55°C ~ 150°C (TJ)" H 6300 4550 50  0001 C CNN "Operating Temperature"
F 17 "8-PowerVDFN" H 6300 4550 50  0001 C CNN "Package / Case"
F 18 "Not For New Designs" H 6300 4550 50  0001 C CNN "Part Status"
F 19 "2.6W (Ta)" H 6300 4550 50  0001 C CNN "Power Dissipation (Max)"
F 20 "9 mOhm @ 11A, 0V" H 6300 4550 50  0001 C CNN "Rds On (Max) @ Id, Vgs"
F 21 "NexFET™" H 6300 4550 50  0001 C CNN "Series"
F 22 "Digi-Key" H 6300 4550 50  0001 C CNN "Supplier 1"
F 23 "8-SON (3.3x3.3)" H 6300 4550 50  0001 C CNN "Supplier Device Package"
F 24 "296-35025-1-ND" H 6300 4550 50  0001 C CNN "Supplier Part Number 1"
F 25 "MOSFET (Metal Oxide)" H 6300 4550 50  0001 C CNN "Technology"
F 26 "±20V" H 6300 4550 50  0001 C CNN "Vgs (Max)"
F 27 "2.1V @ 250µA" H 6300 4550 50  0001 C CNN "Vgs(th) (Max) @ Id"
	1    6300 4550
	-1   0    0    -1  
$EndComp
$Comp
L SFUSat:CSD17551Q3A Q2
U 1 1 5C30A9C9
P 7450 4750
F 0 "Q2" H 7450 5175 50  0000 C CNN
F 1 "CSD17551Q3A" H 7450 5084 50  0000 C CNN
F 2 "SFUSat:8-PowerWDFN" H 7450 4750 50  0001 C CNN
F 3 "" H 7450 4750 50  0001 C CNN
F 4 "Discrete Semiconductor Products - Transistors - FETs, MOSFETs - Single" H 7450 4750 50  0001 C CNN "Categories"
F 5 "12A (Tc)" H 7450 4750 50  0001 C CNN "Current - Continuous Drain (Id) @ 25°C"
F 6 "30V" H 7450 4750 50  0001 C CNN "Drain to Source Voltage (Vdss)"
F 7 "4.5V, 0V" H 7450 4750 50  0001 C CNN "Drive Voltage (Max Rds On,  Min Rds On)"
F 8 "-" H 7450 4750 50  0001 C CNN "FET Feature"
F 9 "N-Channel" H 7450 4750 50  0001 C CNN "FET Type"
F 10 "7.8nC @ 4.5V" H 7450 4750 50  0001 C CNN "Gate Charge (Qg) (Max) @ Vgs"
F 11 "1370pF @ 15V" H 7450 4750 50  0001 C CNN "Input Capacitance (Ciss) (Max) @ Vds"
F 12 "Lead free / RoHS Compliant" H 7450 4750 50  0001 C CNN "Lead Free Status / RoHS Status"
F 13 "Texas Instruments" H 7450 4750 50  0001 C CNN "Manufacturer 1"
F 14 "CSD17551Q3A" H 7450 4750 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "Surface Mount" H 7450 4750 50  0001 C CNN "Mounting Type"
F 16 "-55°C ~ 150°C (TJ)" H 7450 4750 50  0001 C CNN "Operating Temperature"
F 17 "8-PowerVDFN" H 7450 4750 50  0001 C CNN "Package / Case"
F 18 "Not For New Designs" H 7450 4750 50  0001 C CNN "Part Status"
F 19 "2.6W (Ta)" H 7450 4750 50  0001 C CNN "Power Dissipation (Max)"
F 20 "9 mOhm @ 11A, 0V" H 7450 4750 50  0001 C CNN "Rds On (Max) @ Id, Vgs"
F 21 "NexFET™" H 7450 4750 50  0001 C CNN "Series"
F 22 "Digi-Key" H 7450 4750 50  0001 C CNN "Supplier 1"
F 23 "8-SON (3.3x3.3)" H 7450 4750 50  0001 C CNN "Supplier Device Package"
F 24 "296-35025-1-ND" H 7450 4750 50  0001 C CNN "Supplier Part Number 1"
F 25 "MOSFET (Metal Oxide)" H 7450 4750 50  0001 C CNN "Technology"
F 26 "±20V" H 7450 4750 50  0001 C CNN "Vgs (Max)"
F 27 "2.1V @ 250µA" H 7450 4750 50  0001 C CNN "Vgs(th) (Max) @ Id"
	1    7450 4750
	1    0    0    -1  
$EndComp
$Comp
L SFUSat:CSD17551Q3A Q3
U 1 1 5C30AAB3
P 8450 4750
F 0 "Q3" H 8450 5175 50  0000 C CNN
F 1 "CSD17551Q3A" H 8450 5084 50  0000 C CNN
F 2 "SFUSat:8-PowerWDFN" H 8450 4750 50  0001 C CNN
F 3 "" H 8450 4750 50  0001 C CNN
F 4 "Discrete Semiconductor Products - Transistors - FETs, MOSFETs - Single" H 8450 4750 50  0001 C CNN "Categories"
F 5 "12A (Tc)" H 8450 4750 50  0001 C CNN "Current - Continuous Drain (Id) @ 25°C"
F 6 "30V" H 8450 4750 50  0001 C CNN "Drain to Source Voltage (Vdss)"
F 7 "4.5V, 0V" H 8450 4750 50  0001 C CNN "Drive Voltage (Max Rds On,  Min Rds On)"
F 8 "-" H 8450 4750 50  0001 C CNN "FET Feature"
F 9 "N-Channel" H 8450 4750 50  0001 C CNN "FET Type"
F 10 "7.8nC @ 4.5V" H 8450 4750 50  0001 C CNN "Gate Charge (Qg) (Max) @ Vgs"
F 11 "1370pF @ 15V" H 8450 4750 50  0001 C CNN "Input Capacitance (Ciss) (Max) @ Vds"
F 12 "Lead free / RoHS Compliant" H 8450 4750 50  0001 C CNN "Lead Free Status / RoHS Status"
F 13 "Texas Instruments" H 8450 4750 50  0001 C CNN "Manufacturer 1"
F 14 "CSD17551Q3A" H 8450 4750 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "Surface Mount" H 8450 4750 50  0001 C CNN "Mounting Type"
F 16 "-55°C ~ 150°C (TJ)" H 8450 4750 50  0001 C CNN "Operating Temperature"
F 17 "8-PowerVDFN" H 8450 4750 50  0001 C CNN "Package / Case"
F 18 "Not For New Designs" H 8450 4750 50  0001 C CNN "Part Status"
F 19 "2.6W (Ta)" H 8450 4750 50  0001 C CNN "Power Dissipation (Max)"
F 20 "9 mOhm @ 11A, 0V" H 8450 4750 50  0001 C CNN "Rds On (Max) @ Id, Vgs"
F 21 "NexFET™" H 8450 4750 50  0001 C CNN "Series"
F 22 "Digi-Key" H 8450 4750 50  0001 C CNN "Supplier 1"
F 23 "8-SON (3.3x3.3)" H 8450 4750 50  0001 C CNN "Supplier Device Package"
F 24 "296-35025-1-ND" H 8450 4750 50  0001 C CNN "Supplier Part Number 1"
F 25 "MOSFET (Metal Oxide)" H 8450 4750 50  0001 C CNN "Technology"
F 26 "±20V" H 8450 4750 50  0001 C CNN "Vgs (Max)"
F 27 "2.1V @ 250µA" H 8450 4750 50  0001 C CNN "Vgs(th) (Max) @ Id"
	1    8450 4750
	-1   0    0    -1  
$EndComp
$Comp
L SFUSat:CSD17551Q3A Q4
U 1 1 5C30AB9F
P 10300 5250
F 0 "Q4" H 10300 5675 50  0000 C CNN
F 1 "CSD17551Q3A" H 10300 5584 50  0000 C CNN
F 2 "SFUSat:8-PowerWDFN" H 10300 5250 50  0001 C CNN
F 3 "" H 10300 5250 50  0001 C CNN
F 4 "Discrete Semiconductor Products - Transistors - FETs, MOSFETs - Single" H 10300 5250 50  0001 C CNN "Categories"
F 5 "12A (Tc)" H 10300 5250 50  0001 C CNN "Current - Continuous Drain (Id) @ 25°C"
F 6 "30V" H 10300 5250 50  0001 C CNN "Drain to Source Voltage (Vdss)"
F 7 "4.5V, 0V" H 10300 5250 50  0001 C CNN "Drive Voltage (Max Rds On,  Min Rds On)"
F 8 "-" H 10300 5250 50  0001 C CNN "FET Feature"
F 9 "N-Channel" H 10300 5250 50  0001 C CNN "FET Type"
F 10 "7.8nC @ 4.5V" H 10300 5250 50  0001 C CNN "Gate Charge (Qg) (Max) @ Vgs"
F 11 "1370pF @ 15V" H 10300 5250 50  0001 C CNN "Input Capacitance (Ciss) (Max) @ Vds"
F 12 "Lead free / RoHS Compliant" H 10300 5250 50  0001 C CNN "Lead Free Status / RoHS Status"
F 13 "Texas Instruments" H 10300 5250 50  0001 C CNN "Manufacturer 1"
F 14 "CSD17551Q3A" H 10300 5250 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "Surface Mount" H 10300 5250 50  0001 C CNN "Mounting Type"
F 16 "-55°C ~ 150°C (TJ)" H 10300 5250 50  0001 C CNN "Operating Temperature"
F 17 "8-PowerVDFN" H 10300 5250 50  0001 C CNN "Package / Case"
F 18 "Not For New Designs" H 10300 5250 50  0001 C CNN "Part Status"
F 19 "2.6W (Ta)" H 10300 5250 50  0001 C CNN "Power Dissipation (Max)"
F 20 "9 mOhm @ 11A, 0V" H 10300 5250 50  0001 C CNN "Rds On (Max) @ Id, Vgs"
F 21 "NexFET™" H 10300 5250 50  0001 C CNN "Series"
F 22 "Digi-Key" H 10300 5250 50  0001 C CNN "Supplier 1"
F 23 "8-SON (3.3x3.3)" H 10300 5250 50  0001 C CNN "Supplier Device Package"
F 24 "296-35025-1-ND" H 10300 5250 50  0001 C CNN "Supplier Part Number 1"
F 25 "MOSFET (Metal Oxide)" H 10300 5250 50  0001 C CNN "Technology"
F 26 "±20V" H 10300 5250 50  0001 C CNN "Vgs (Max)"
F 27 "2.1V @ 250µA" H 10300 5250 50  0001 C CNN "Vgs(th) (Max) @ Id"
	1    10300 5250
	1    0    0    -1  
$EndComp
$Comp
L SFUSat:CSD25402Q3A Q5
U 1 1 5C30AD2D
P 10450 6200
F 0 "Q5" H 10450 6625 50  0000 C CNN
F 1 "CSD25402Q3A" H 10450 6534 50  0000 C CNN
F 2 "SFUSat:8-PowerWDFN" H 10450 6200 50  0001 C CNN
F 3 "" H 10450 6200 50  0001 C CNN
F 4 "Discrete Semiconductor Products - Transistors - FETs, MOSFETs - Single" H 10450 6200 50  0001 C CNN "Categories"
F 5 "76A (Tc)" H 10450 6200 50  0001 C CNN "Current - Continuous Drain (Id) @ 25°C"
F 6 "20V" H 10450 6200 50  0001 C CNN "Drain to Source Voltage (Vdss)"
F 7 "1.8V, 4.5V" H 10450 6200 50  0001 C CNN "Drive Voltage (Max Rds On,  Min Rds On)"
F 8 "-" H 10450 6200 50  0001 C CNN "FET Feature"
F 9 "P-Channel" H 10450 6200 50  0001 C CNN "FET Type"
F 10 "9.7nC @ 4.5V" H 10450 6200 50  0001 C CNN "Gate Charge (Qg) (Max) @ Vgs"
F 11 "1790pF @ 10V" H 10450 6200 50  0001 C CNN "Input Capacitance (Ciss) (Max) @ Vds"
F 12 "Lead free / RoHS Compliant" H 10450 6200 50  0001 C CNN "Lead Free Status / RoHS Status"
F 13 "Texas Instruments" H 10450 6200 50  0001 C CNN "Manufacturer 1"
F 14 "CSD25402Q3A" H 10450 6200 50  0001 C CNN "Manufacturer Part Number 1"
F 15 "17 Weeks" H 10450 6200 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 16 "Surface Mount" H 10450 6200 50  0001 C CNN "Mounting Type"
F 17 "-55°C ~ 150°C (TJ)" H 10450 6200 50  0001 C CNN "Operating Temperature"
F 18 "8-PowerVDFN" H 10450 6200 50  0001 C CNN "Package / Case"
F 19 "Active" H 10450 6200 50  0001 C CNN "Part Status"
F 20 "2.8W (Ta), 69W (Tc)" H 10450 6200 50  0001 C CNN "Power Dissipation (Max)"
F 21 "8.9 mOhm @ 10A, 4.5V" H 10450 6200 50  0001 C CNN "Rds On (Max) @ Id, Vgs"
F 22 "NexFET™" H 10450 6200 50  0001 C CNN "Series"
F 23 "Digi-Key" H 10450 6200 50  0001 C CNN "Supplier 1"
F 24 "8-VSON (3.3x3.3)" H 10450 6200 50  0001 C CNN "Supplier Device Package"
F 25 "296-38916-1-ND" H 10450 6200 50  0001 C CNN "Supplier Part Number 1"
F 26 "MOSFET (Metal Oxide)" H 10450 6200 50  0001 C CNN "Technology"
F 27 "±12V" H 10450 6200 50  0001 C CNN "Vgs (Max)"
F 28 "1.15V @ 250µA" H 10450 6200 50  0001 C CNN "Vgs(th) (Max) @ Id"
	1    10450 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4350 5900 4450
Connection ~ 5900 4450
Wire Wire Line
	5900 4450 5900 4550
Connection ~ 5900 4550
Wire Wire Line
	5900 4550 5900 4650
Connection ~ 5900 4650
Wire Wire Line
	5900 4650 5900 4750
Wire Wire Line
	6700 4550 6700 4450
Connection ~ 6700 4450
Wire Wire Line
	6700 4450 6700 4350
Wire Wire Line
	6750 5250 6750 4950
Wire Wire Line
	6750 4950 5900 4950
Wire Wire Line
	5900 4950 5900 4750
Connection ~ 6750 5250
Connection ~ 5900 4750
Wire Wire Line
	6700 4750 6900 4750
Wire Wire Line
	6900 4750 6900 6800
Wire Wire Line
	6950 5250 6950 4550
Wire Wire Line
	6950 4550 6700 4550
Connection ~ 7250 5250
Connection ~ 6700 4550
$Comp
L power:GND #PWR07
U 1 1 5C3E5935
P 7050 4750
F 0 "#PWR07" H 7050 4500 50  0001 C CNN
F 1 "GND" H 7055 4577 50  0001 C CNN
F 2 "" H 7050 4750 50  0001 C CNN
F 3 "" H 7050 4750 50  0001 C CNN
	1    7050 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4750 7050 4650
Connection ~ 7050 4750
Wire Wire Line
	7050 4550 7050 4650
Connection ~ 7050 4650
Wire Wire Line
	7850 4550 7850 4650
Wire Wire Line
	7850 4650 7850 4750
Connection ~ 7850 4650
Wire Wire Line
	7850 4750 7850 4850
Connection ~ 7850 4750
Wire Wire Line
	7850 4950 7850 4850
Connection ~ 7850 4850
Wire Wire Line
	7850 4950 7850 5050
Wire Wire Line
	7850 5050 7250 5050
Wire Wire Line
	7250 5050 7250 5250
Connection ~ 7850 4950
Wire Wire Line
	6950 5300 7050 5300
Wire Wire Line
	7050 5300 7050 5250
Wire Wire Line
	6950 5300 6950 6600
NoConn ~ 7050 5250
$Comp
L power:GND #PWR010
U 1 1 5C482691
P 8850 4750
F 0 "#PWR010" H 8850 4500 50  0001 C CNN
F 1 "GND" H 8855 4577 50  0001 C CNN
F 2 "" H 8850 4750 50  0001 C CNN
F 3 "" H 8850 4750 50  0001 C CNN
	1    8850 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 4550 8850 4650
Wire Wire Line
	8850 4750 8850 4650
Connection ~ 8850 4750
Connection ~ 8850 4650
Wire Wire Line
	8050 4550 8050 4650
Connection ~ 8050 4650
Wire Wire Line
	8050 4650 8050 4750
Connection ~ 8050 4750
Wire Wire Line
	8050 4750 8050 4850
Connection ~ 8050 4850
Wire Wire Line
	8050 4850 8050 4950
Wire Wire Line
	8050 4550 8050 4250
Wire Wire Line
	8050 4250 8900 4250
Wire Wire Line
	8900 4250 8900 4550
Wire Wire Line
	8900 4550 9000 4550
Wire Wire Line
	9000 4550 9000 4800
Wire Wire Line
	9000 4800 9500 4800
Wire Wire Line
	9500 4800 9500 5250
Connection ~ 8050 4550
Connection ~ 9500 5250
Wire Wire Line
	8850 4950 9100 4950
Wire Wire Line
	9100 4950 9100 5250
Wire Wire Line
	9100 5850 9300 5850
Wire Wire Line
	9400 5850 9400 6600
NoConn ~ 9100 5250
NoConn ~ 9300 5850
Wire Wire Line
	9900 5050 9900 5150
Connection ~ 9900 5150
Wire Wire Line
	9900 5150 9900 5250
Wire Wire Line
	10700 5050 10700 5150
Connection ~ 10700 5150
Wire Wire Line
	10700 5150 10700 5250
Connection ~ 10700 5250
Wire Wire Line
	10700 5250 10700 5350
Connection ~ 10700 5350
Wire Wire Line
	10700 5350 10700 5450
Wire Wire Line
	10950 5500 10900 5500
Connection ~ 10700 5450
Connection ~ 9900 5250
Wire Wire Line
	9900 6800 9250 6800
Wire Wire Line
	10700 5500 10700 5450
Wire Wire Line
	9900 5450 9900 6800
Wire Wire Line
	9500 5250 9900 5250
Wire Wire Line
	10050 6200 10050 6100
Connection ~ 10050 6100
Wire Wire Line
	10050 6100 10050 6000
Wire Wire Line
	10850 6000 10850 6100
Connection ~ 10850 6100
Wire Wire Line
	10850 6100 10850 6200
Connection ~ 10850 6200
Wire Wire Line
	10850 6200 10850 6300
Connection ~ 10850 6300
Wire Wire Line
	10850 6300 10850 6400
Wire Wire Line
	10000 7000 10000 5550
Wire Wire Line
	10000 5550 10900 5550
Wire Wire Line
	10900 5550 10900 5500
Wire Wire Line
	10000 7000 9250 7000
Connection ~ 10900 5500
Wire Wire Line
	10900 5500 10700 5500
Wire Wire Line
	10950 5600 10050 5600
Wire Wire Line
	10050 5600 10050 6000
Connection ~ 10050 6000
Wire Wire Line
	10900 4900 10900 5500
Wire Wire Line
	10850 6000 11000 6000
Wire Wire Line
	11000 6000 11000 5500
Wire Wire Line
	11000 5500 11500 5500
Connection ~ 10850 6000
Wire Wire Line
	10050 6400 10050 7200
Wire Wire Line
	13550 8550 13550 8350
Wire Wire Line
	13050 8500 13050 8400
Wire Wire Line
	12950 8400 12950 8350
Wire Wire Line
	13000 8450 13000 8400
Wire Wire Line
	13000 8350 13050 8350
Wire Wire Line
	11150 8450 13000 8450
NoConn ~ 13000 8400
Wire Wire Line
	11000 8650 11050 8650
Wire Wire Line
	13650 8650 13650 8350
NoConn ~ 11050 8650
NoConn ~ 11100 8650
NoConn ~ 11150 8650
NoConn ~ 11200 8650
Connection ~ 6050 5250
Wire Wire Line
	7050 5250 7050 4950
Wire Wire Line
	7200 9000 7200 9200
Wire Wire Line
	7200 9200 7200 9600
Wire Wire Line
	7200 9600 7200 10300
Wire Wire Line
	7250 9200 7250 9600
Wire Wire Line
	7250 9600 7250 10250
Wire Wire Line
	7350 9600 7350 10150
Wire Wire Line
	9100 5250 9100 5850
Wire Wire Line
	9300 5850 9400 5850
Wire Wire Line
	11050 8650 11100 8650
Wire Wire Line
	11150 8550 11200 8550
Wire Wire Line
	11150 8650 11150 10250
Wire Wire Line
	11200 8550 13550 8550
Wire Wire Line
	11200 8650 11200 10300
Wire Wire Line
	13000 8400 13000 8350
$Comp
L SFUSat-power:ACS722 U3
U 1 1 5C00C1F6
P 4850 4250
F 0 "U3" V 4896 3772 50  0000 R CNN
F 1 "ACS722" V 4805 3772 50  0000 R CNN
F 2 "SFUSat:SOIC-8" H 4750 3650 50  0001 C CNN
F 3 "" H 4850 4250 50  0001 C CNN
F 4 "620-1638-1-ND" H 0   0   50  0001 C CNN "Digikey #"
	1    4850 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 5250 6050 5250
Wire Wire Line
	4800 4800 4600 4800
Text GLabel 4600 3700 1    50   Input ~ 0
3v3
$Comp
L power:GND #PWR022
U 1 1 5C1A99BE
P 5200 3700
F 0 "#PWR022" H 5200 3450 50  0001 C CNN
F 1 "GND" H 5205 3527 50  0001 C CNN
F 2 "" H 5200 3700 50  0001 C CNN
F 3 "" H 5200 3700 50  0001 C CNN
	1    5200 3700
	-1   0    0    1   
$EndComp
Text GLabel 4800 3300 1    50   Output ~ 0
I_IN_SENSE
Text GLabel 12950 6150 1    50   Input ~ 0
I_IN_SENSE
Text GLabel 12850 6150 1    50   Input ~ 0
I_BAT_SENSE
Text GLabel 12750 6150 1    50   Input ~ 0
I_SYS_SENSE
$Comp
L SFUSat-power:ACS722 U4
U 1 1 5C212BC6
P 13050 2300
F 0 "U4" V 13096 1822 50  0000 R CNN
F 1 "ACS722" V 13005 1822 50  0000 R CNN
F 2 "SFUSat:SOIC-8" H 12950 1700 50  0001 C CNN
F 3 "" H 13050 2300 50  0001 C CNN
F 4 "620-1638-1-ND" H 0   -1350 50  0001 C CNN "Digikey #"
	1    13050 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13000 2850 12800 2850
Text GLabel 12800 1750 1    50   Input ~ 0
3v3
$Comp
L power:GND #PWR023
U 1 1 5C212BD5
P 13400 1750
F 0 "#PWR023" H 13400 1500 50  0001 C CNN
F 1 "GND" H 13405 1577 50  0001 C CNN
F 2 "" H 13400 1750 50  0001 C CNN
F 3 "" H 13400 1750 50  0001 C CNN
	1    13400 1750
	-1   0    0    1   
$EndComp
Text GLabel 13000 1200 1    50   Output ~ 0
I_SYS_SENSE
$Comp
L SFUSat-power:ACS722 U5
U 1 1 5C228391
P 14750 2300
F 0 "U5" V 14796 1822 50  0000 R CNN
F 1 "ACS722" V 14705 1822 50  0000 R CNN
F 2 "SFUSat:SOIC-8" H 14650 1700 50  0001 C CNN
F 3 "" H 14750 2300 50  0001 C CNN
F 4 "620-1638-1-ND" H 0   -1350 50  0001 C CNN "Digikey #"
	1    14750 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14700 2850 14500 2850
Text GLabel 14500 1750 1    50   Input ~ 0
3v3
$Comp
L power:GND #PWR024
U 1 1 5C2283A0
P 15100 1750
F 0 "#PWR024" H 15100 1500 50  0001 C CNN
F 1 "GND" H 15105 1577 50  0001 C CNN
F 2 "" H 15100 1750 50  0001 C CNN
F 3 "" H 15100 1750 50  0001 C CNN
	1    15100 1750
	-1   0    0    1   
$EndComp
Text GLabel 14700 1200 1    50   Output ~ 0
I_BAT_SENSE
Wire Wire Line
	13000 4900 12650 4900
Connection ~ 12650 4900
Wire Wire Line
	13300 5500 13550 5500
Wire Wire Line
	13050 5500 13050 5250
Connection ~ 12000 5500
Wire Wire Line
	13300 5500 13300 5300
Wire Wire Line
	13300 5300 14950 5300
Wire Wire Line
	14950 5300 14950 4900
Wire Wire Line
	4800 3700 4800 3400
Wire Wire Line
	13000 1750 13000 1400
Wire Wire Line
	14700 1750 14700 1350
$Comp
L Connector:TestPoint TP14
U 1 1 5C30C4FC
P 13000 1400
F 0 "TP14" H 13058 1474 50  0000 L CNN
F 1 "TestPoint" H 13058 1429 50  0001 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Big" H 13200 1400 50  0001 C CNN
F 3 "~" H 13200 1400 50  0001 C CNN
	1    13000 1400
	0    1    1    0   
$EndComp
Connection ~ 13000 1400
Wire Wire Line
	13000 1400 13000 1200
$Comp
L Connector:TestPoint TP15
U 1 1 5C30C838
P 14700 1350
F 0 "TP15" H 14758 1424 50  0000 L CNN
F 1 "TestPoint" H 14758 1379 50  0001 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Big" H 14900 1350 50  0001 C CNN
F 3 "~" H 14900 1350 50  0001 C CNN
	1    14700 1350
	0    1    1    0   
$EndComp
Connection ~ 14700 1350
Wire Wire Line
	14700 1350 14700 1200
$Comp
L Connector:TestPoint TP8
U 1 1 5C30C91E
P 4800 3400
F 0 "TP8" H 4858 3474 50  0000 L CNN
F 1 "TestPoint" H 4858 3429 50  0001 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Big" H 5000 3400 50  0001 C CNN
F 3 "~" H 5000 3400 50  0001 C CNN
	1    4800 3400
	0    1    1    0   
$EndComp
Connection ~ 4800 3400
Wire Wire Line
	4800 3400 4800 3300
Wire Wire Line
	12950 8400 13050 8400
Wire Wire Line
	11150 8450 11150 8650
Wire Wire Line
	11200 8500 11200 8650
Wire Wire Line
	11100 8650 13650 8650
Wire Wire Line
	11100 8550 11100 10200
Wire Wire Line
	11050 8400 11050 10150
Wire Wire Line
	6950 5250 7250 5250
Wire Wire Line
	8650 5250 9300 5250
Wire Wire Line
	9300 5250 9300 6400
Wire Wire Line
	13950 4450 13500 4450
Wire Wire Line
	13500 4450 13500 4900
Connection ~ 8950 3950
Connection ~ 13500 4900
Wire Wire Line
	13500 4900 13550 4900
Wire Wire Line
	12000 5500 13050 5500
Wire Wire Line
	10900 4900 11400 4900
Wire Wire Line
	6700 9200 7300 9200
Wire Wire Line
	6700 9000 7250 9000
Wire Wire Line
	6700 8800 7200 8800
Wire Wire Line
	6700 8600 7450 8600
Wire Wire Line
	6700 9600 7400 9600
$Comp
L SFUSat-power:Wurth61301011821 J2
U 1 1 5C08B42A
P 13050 6150
F 0 "J2" V 13900 5900 60  0000 L CNN
F 1 "Wurth61301011821" V 14150 5500 60  0000 L CNN
F 2 "SFUSat:Wurth61301011821" H 14150 6390 60  0001 C CNN
F 3 "" H 13050 6150 60  0000 C CNN
	1    13050 6150
	0    1    1    0   
$EndComp
$Comp
L SFUSat-power:Wurth61301011821 J3
U 1 1 5C08C0BB
P 13550 8350
F 0 "J3" V 14900 8150 60  0000 L CNN
F 1 "Wurth61301011821" V 14650 7700 60  0000 L CNN
F 2 "SFUSat:Wurth61301011821" H 14650 8590 60  0001 C CNN
F 3 "" H 13550 8350 60  0000 C CNN
	1    13550 8350
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D1
U 1 1 5C077F2C
P 6650 10050
F 0 "D1" H 6641 10357 50  0000 C CNN
F 1 "APT1608ZGCK" H 6641 10266 50  0000 C CNN
F 2 "SFUSat-res:R_0603" H 6650 10350 50  0001 C CNN
F 3 "~" H 6650 10050 50  0001 C CNN
F 4 "754-1790-1-ND" H 6641 10175 50  0000 C CNN "Digikey #"
	1    6650 10050
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-res:R_160R0_5%_0.1W_0603 R23
U 1 1 5C0784AC
P 6150 10200
F 0 "R23" V 6196 10130 50  0000 R CNN
F 1 "R_160R0_5%_0.1W_0603" V 6105 10130 50  0000 R CNN
F 2 "SFUSat-res:R_0603" H 6150 10200 50  0001 C CNN
F 3 "" H 6150 10200 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 6150 10200 50  0001 C CNN "Categories"
F 5 "Thick Film" H 6150 10200 50  0001 C CNN "Composition"
F 6 "-" H 6150 10200 50  0001 C CNN "Failure Rate"
F 7 "Moisture Resistant" H 6150 10200 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 6150 10200 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 6150 10200 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Yageo" H 6150 10200 50  0001 C CNN "Manufacturer 1"
F 11 "RC0603JR-07160RL" H 6150 10200 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "23 Weeks" H 6150 10200 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 6150 10200 50  0001 C CNN "Number of Terminations"
F 14 "-55°C ~ 155°C" H 6150 10200 50  0001 C CNN "Operating Temperature"
F 15 "0603 (1608 Metric)" H 6150 10200 50  0001 C CNN "Package / Case"
F 16 "Active" H 6150 10200 50  0001 C CNN "Part Status"
F 17 "0.1W, 1/10W" H 6150 10200 50  0001 C CNN "Power (Watts)"
F 18 "160 Ohms" H 6150 10200 50  0001 C CNN "Resistance"
F 19 "RC" H 6150 10200 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 6150 10200 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 6150 10200 50  0001 C CNN "Supplier 1"
F 22 "0603" H 6150 10200 50  0001 C CNN "Supplier Device Package"
F 23 "311-160GRCT-ND" H 6150 10200 50  0001 C CNN "Supplier Part Number 1"
F 24 "±100ppm/°C" H 6150 10200 50  0001 C CNN "Temperature Coefficient"
F 25 "±5%" H 6150 10200 50  0001 C CNN "Tolerance"
	1    6150 10200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5C078A8D
P 6150 10350
F 0 "#PWR025" H 6150 10100 50  0001 C CNN
F 1 "GND" V 6155 10222 50  0001 R CNN
F 2 "" H 6150 10350 50  0001 C CNN
F 3 "" H 6150 10350 50  0001 C CNN
	1    6150 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 10050 6500 10050
Wire Wire Line
	6800 10050 7300 10050
NoConn ~ 7250 10050
NoConn ~ 7200 10050
Connection ~ 7300 10050
Wire Wire Line
	7300 10050 7300 10200
Wire Wire Line
	7300 9600 7300 10050
$Comp
L Device:LED D2
U 1 1 5C0E3A3B
P 10350 2700
F 0 "D2" H 10341 3007 50  0000 C CNN
F 1 "APT1608ZGCK" H 10341 2916 50  0000 C CNN
F 2 "SFUSat-res:R_0603" H 10350 3000 50  0001 C CNN
F 3 "~" H 10350 2700 50  0001 C CNN
F 4 "754-1790-1-ND" H 10341 2825 50  0000 C CNN "Digikey #"
	1    10350 2700
	1    0    0    -1  
$EndComp
$Comp
L SFUSat-res:R_160R0_5%_0.1W_0603 R24
U 1 1 5C0E3A58
P 9850 2850
F 0 "R24" V 9896 2780 50  0000 R CNN
F 1 "R_160R0_5%_0.1W_0603" V 9805 2780 50  0000 R CNN
F 2 "SFUSat-res:R_0603" H 9850 2850 50  0001 C CNN
F 3 "" H 9850 2850 50  0001 C CNN
F 4 "Resistors - Chip Resistor - Surface Mount" H 9850 2850 50  0001 C CNN "Categories"
F 5 "Thick Film" H 9850 2850 50  0001 C CNN "Composition"
F 6 "-" H 9850 2850 50  0001 C CNN "Failure Rate"
F 7 "Moisture Resistant" H 9850 2850 50  0001 C CNN "Features"
F 8 "0.022\" (0.55mm)" H 9850 2850 50  0001 C CNN "Height - Seated (Max)"
F 9 "Lead free / RoHS Compliant" H 9850 2850 50  0001 C CNN "Lead Free Status / RoHS Status"
F 10 "Yageo" H 9850 2850 50  0001 C CNN "Manufacturer 1"
F 11 "RC0603JR-07160RL" H 9850 2850 50  0001 C CNN "Manufacturer Part Number 1"
F 12 "23 Weeks" H 9850 2850 50  0001 C CNN "Manufacturer Standard Lead Time 1"
F 13 "2" H 9850 2850 50  0001 C CNN "Number of Terminations"
F 14 "-55°C ~ 155°C" H 9850 2850 50  0001 C CNN "Operating Temperature"
F 15 "0603 (1608 Metric)" H 9850 2850 50  0001 C CNN "Package / Case"
F 16 "Active" H 9850 2850 50  0001 C CNN "Part Status"
F 17 "0.1W, 1/10W" H 9850 2850 50  0001 C CNN "Power (Watts)"
F 18 "160 Ohms" H 9850 2850 50  0001 C CNN "Resistance"
F 19 "RC" H 9850 2850 50  0001 C CNN "Series"
F 20 "0.063\" L x 0.032\" W (1.60mm x 0.80mm)" H 9850 2850 50  0001 C CNN "Size / Dimension"
F 21 "Digi-Key" H 9850 2850 50  0001 C CNN "Supplier 1"
F 22 "0603" H 9850 2850 50  0001 C CNN "Supplier Device Package"
F 23 "311-160GRCT-ND" H 9850 2850 50  0001 C CNN "Supplier Part Number 1"
F 24 "±100ppm/°C" H 9850 2850 50  0001 C CNN "Temperature Coefficient"
F 25 "±5%" H 9850 2850 50  0001 C CNN "Tolerance"
	1    9850 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5C0E3A5F
P 9850 3000
F 0 "#PWR026" H 9850 2750 50  0001 C CNN
F 1 "GND" V 9855 2872 50  0001 R CNN
F 2 "" H 9850 3000 50  0001 C CNN
F 3 "" H 9850 3000 50  0001 C CNN
	1    9850 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2700 10200 2700
Wire Wire Line
	10500 2700 11000 2700
Text GLabel 11000 2700 1    50   Input ~ 0
3v3
$Comp
L SFUSat:TSW-102-07-F-D J6
U 1 1 5C07A1E2
P 4950 4800
F 0 "J6" V 5652 5028 60  0000 L CNN
F 1 "TSW-102-07-F-D" V 5758 5028 60  0000 L CNN
F 2 "SFUSat:TSW-102-07-F-D" H 5750 5040 60  0001 C CNN
F 3 "" H 4950 4800 60  0000 C CNN
F 4 "SAM10843-ND" V 5856 5028 50  0000 L CNN "Digikey #"
	1    4950 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 4800 5200 4800
Wire Wire Line
	4800 4800 4850 4800
Connection ~ 4800 4800
Wire Wire Line
	4950 4800 5000 4800
Connection ~ 5000 4800
Wire Wire Line
	4950 6400 5950 6400
Wire Wire Line
	5950 5250 5950 6400
Wire Wire Line
	4850 6400 3950 6400
Wire Wire Line
	3950 6400 3950 6050
Connection ~ 3950 6050
$Comp
L SFUSat:TSW-102-07-F-D J7
U 1 1 5C0CE5ED
P 13150 2850
F 0 "J7" V 13852 3078 60  0000 L CNN
F 1 "TSW-102-07-F-D" V 13958 3078 60  0000 L CNN
F 2 "SFUSat:TSW-102-07-F-D" H 13950 3090 60  0001 C CNN
F 3 "" H 13150 2850 60  0000 C CNN
F 4 "SAM10843-ND" V 14056 3078 50  0000 L CNN "Digikey #"
	1    13150 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	13950 600  8950 600 
Wire Wire Line
	8950 600  8950 3950
Wire Wire Line
	13950 600  13950 4450
$Comp
L SFUSat:TSW-102-07-F-D J8
U 1 1 5C10F207
P 14850 2850
F 0 "J8" V 15552 3078 60  0000 L CNN
F 1 "TSW-102-07-F-D" V 15658 3078 60  0000 L CNN
F 2 "SFUSat:TSW-102-07-F-D" H 15650 3090 60  0001 C CNN
F 3 "" H 14850 2850 60  0000 C CNN
F 4 "SAM10843-ND" V 15756 3078 50  0000 L CNN "Digikey #"
	1    14850 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	13200 2850 13400 2850
Wire Wire Line
	14900 2850 15100 2850
Wire Wire Line
	13000 2850 13050 2850
Connection ~ 13000 2850
Wire Wire Line
	13150 2850 13200 2850
Connection ~ 13200 2850
Wire Wire Line
	14700 2850 14750 2850
Connection ~ 14700 2850
Wire Wire Line
	14850 2850 14900 2850
Connection ~ 14900 2850
Wire Wire Line
	13000 4900 13000 4650
Wire Wire Line
	13000 4650 13150 4650
Wire Wire Line
	13150 4650 13150 4450
Wire Wire Line
	13100 4900 13100 4450
Wire Wire Line
	13100 4450 13050 4450
Wire Wire Line
	13100 4900 13500 4900
NoConn ~ 13100 4650
Wire Wire Line
	14750 4900 14750 4450
Wire Wire Line
	14750 4900 14950 4900
Wire Wire Line
	14850 4450 14850 5250
Wire Wire Line
	13050 5250 14850 5250
NoConn ~ 14850 4900
$Comp
L SFUSat:PJ-102BH J1
U 1 1 5C08E4C3
P 3250 5350
F 0 "J1" H 3219 5690 50  0000 C CNN
F 1 "PJ-102BH" H 3219 5599 50  0000 C CNN
F 2 "SFUSat:CUI_PJ-102BH" H 3250 5350 50  0001 L BNN
F 3 "https://www.digikey.ca/product-detail/en/cui-inc/PJ-102BH/CP-102BH-ND/408449?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 3250 5350 50  0001 L BNN
F 4 "2.5 mm Center Pin, 5.0 A, Right Angle, Through Hole, Tapered Pins, Dc Power Jack Connector" H 3250 5350 50  0001 L BNN "Field4"
F 5 "https://www.cui.com/product/interconnect/dc-power-connectors/jacks/2.5-mm-center-pin/pj-102bh?utm_source=snapeda.com&utm_medium=referral&utm_campaign=snapedaBOM" H 3250 5350 50  0001 L BNN "Field5"
F 6 "CUI Inc." H 3250 5350 50  0001 L BNN "Field6"
F 7 "None" H 3250 5350 50  0001 L BNN "Field7"
F 8 "CP-102BH-ND" H 3250 5350 50  0001 L BNN "Field8"
F 9 "PJ-102BH" H 3250 5350 50  0001 L BNN "Field9"
	1    3250 5350
	1    0    0    -1  
$EndComp
Connection ~ 3450 5450
Wire Wire Line
	3450 5450 3450 5350
$Comp
L power:GND #PWR0101
U 1 1 5C0A8B14
P 9300 9800
F 0 "#PWR0101" H 9300 9550 50  0001 C CNN
F 1 "GND" V 9305 9672 50  0001 R CNN
F 2 "" H 9300 9800 50  0001 C CNN
F 3 "" H 9300 9800 50  0001 C CNN
	1    9300 9800
	1    0    0    -1  
$EndComp
Connection ~ 9300 9800
$EndSCHEMATC
